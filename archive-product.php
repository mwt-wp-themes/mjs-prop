<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Landing Page
 */

get_header();
?>


		<?php if ( have_posts() ) : ?>

        <div class="pricing-4" id="pricing-4">
          <div class="container">
            <div class="row">
              <div class="col-md-6 ml-auto mr-auto text-center">
                <h2 class="title">Pick the best plan for you</h2>
                <h4 class="description">You have Free Unlimited Updates and Premium Support on each package.</h4>
                <div class="section-space"></div>
              </div>
            </div>
            <div class="row">

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post(); ?>

        <?php get_template_part( 'loop-templates/archive', 'product' ); ?>

			<?php endwhile;

			//the_posts_navigation(); 
      ?>

              <div class="col-md-4">
                <div class="card card-pricing" data-background-color="orange">
                  <div class="card-body">
                    <h5 class="category">Professional</h5>
                    <div class="icon icon-primary">
                      <i class="now-ui-icons tech_headphones"></i>
                    </div>
                    <h3 class="card-title"><small>$</small>40</h3>
                    <ul>
                      <li>Unlimited MB</li>
                      <li>Unlimited emails</li>
                      <li>Full Support</li>
                    </ul>
                    <a href="#pablo" class="btn btn-neutral btn-primary disabled btn-round">
                    Current Plan
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-pricing card-plain">
                  <div class="card-body">
                    <h5 class="category">Basic</h5>
                    <div class="icon icon-warning">
                      <i class="now-ui-icons tech_headphones"></i>
                    </div>
                    <h3 class="card-title"><small>$</small>20</h3>
                    <ul>
                      <li>1000 MB</li>
                      <li>3 email</li>
                      <li>No Support</li>
                    </ul>
                    <a href="#pablo" class="btn btn-warning btn-round">
                    Upgrade Plan
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

		<?php else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

<?php
get_footer();
