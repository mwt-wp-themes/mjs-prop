/**
 * Custom JS
 * @version v1.1
 */

jQuery(function ($) {
    "use strict";
    
$('body').on('click', '#submitKontak', function (e) {
    var data = {};
    data.nama = $("#kontak_kami_form #nama").val();
    data.email = $("#kontak_kami_form #email").val();
    data.telepon = $("#kontak_kami_form #telepon").val();
    data.kota = $("#kontak_kami_form #kota").val();
    data.pesan = $("#kontak_kami_form #pesan").val();
    data.action = 'mwt_contact_form_action';
    data.security = mjs_object.wp_nonce;
    $.ajax({
        type: 'post',
        url: mjs_object.ajax_url,
        data: data,
        beforeSend: function() {
            $.LoadingOverlay("show");
        },
        error: function () {
            $.LoadingOverlay("hide");
//             $('#message .col-md-12').remove();
//             $('#message').append('<div class="col-md-12"><div class="box-message c-red" style="margin-top: 15px"><i class="zmdi zmdi-alert-circle"></i>Kesalahan server Silakan coba lagi setelah beberapa waktu ...</div></div>');
        },
        success: function (response) {
            var data = $.parseJSON(response);
            $.LoadingOverlay("hide");
            $("#kontak_kami_form input").val('');
            $("#kontak_kami_form textarea").val('');
            alert( "Pesan berhasil terkirim. Terima kasih." );
        }
    });
});
  
$('body').on('click', '#submit-form-peminat', function (e) {
    var data = {};
    data.nama = $("#peminat_nama").val();
    data.email = $("#peminat_email").val();
    data.telp = $("#peminat_hp").val();
    //data.company = $("#peminat_company").val();
    data.kavling = $("input[name=peminat_kavling]:checked").val();
    //data.domisili = $("#peminat_domisili").val();
    data.action = 'mwt_tambah_peminat_action';
    data.security = mjs_object.wp_nonce;
    if( data.nama === '' ) {
      $("#form-peminat-msg").html('<div class="alert alert-danger">Harap isi nama anda!</div>');
      $('html, body').animate({
          scrollTop: $("#form-pemesanan").offset().top
      }, 2000);
    } else if( data.email === '' ) {
      $("#form-peminat-msg").html('<div class="alert alert-danger">Harap isi email anda!</div>');
      $('html, body').animate({
          scrollTop: $("#form-pemesanan").offset().top
      }, 2000);
    } else if( data.telp === '' ) {
      $("#form-peminat-msg").html('<div class="alert alert-danger">Harap isi Nomor HP anda!</div>');
      $('html, body').animate({
          scrollTop: $("#form-pemesanan").offset().top
      }, 2000);
    } else if( data.kavling === '' ) {
      $("#form-peminat-msg").html('<div class="alert alert-danger">Harap pilih kavling!</div>');
      $('html, body').animate({
          scrollTop: $("#form-pemesanan").offset().top
      }, 2000);
    } else {
      $.ajax({
          type: 'post',
          url: mjs_object.ajax_url,
          data: data,
          beforeSend: function() {
              $.LoadingOverlay("show");
          },
          error: function () {
              $.LoadingOverlay("hide");
              $("#form-peminat-msg").html('<div class="alert alert-danger">Terjadi kesalahan!</div>');
          },
          success: function (response) {
              var data = $.parseJSON(response);
              $.LoadingOverlay("hide");
              $('#verifikasiModal').modal();
              $('#submit_id').val(data.submit_id);
          }
      }); 
    }
});
  
$('body').on('click', '#submit-verifikasi-hp', function (e) {
    $("#form-peminat-msg").html('');
    var data = {};
    data.kode_verifikasi = $("#kode_verifikasi").val();
    data.submit_id = $("#submit_id").val();
    data.action = 'mwt_verifikasi_sms_action';
    data.security = mjs_object.wp_nonce;
    if( data.kode_verifikasi === '' ) {
      $("#form-verifikasi-msg").html('<div class="alert alert-danger">Harap masukkan kode verifikasi yang terkirim ke hp anda!</div>');
    } else {
      $.ajax({
          type: 'post',
          url: mjs_object.ajax_url,
          data: data,
          beforeSend: function() {
              $.LoadingOverlay("show");
          },
          error: function () {
              $.LoadingOverlay("hide");
              $("#form-verifikasi-msg").html('<div class="alert alert-danger">Terjadi kesalahan!</div>');
          },
          success: function (response) {
              var data = $.parseJSON(response);
              if( data.error === 1 ) {
                 $("#form-verifikasi-msg").html('<div class="alert alert-danger">'+data.msg+'</div>');
                 $.LoadingOverlay("hide");
               } else {
                  $("#form-verifikasi-wa").remove();
//                   $('#verifikasiModal').modal('hide');
//                   $("#kode_verifikasi").val('');
                  $("#form-verifikasi-msg").html('<div class="alert alert-success">'+data.msg+'</div>');
                  $.LoadingOverlay("hide");
//                   $('html, body').animate({
//                       scrollTop: $("#form-pemesanan").offset().top
//                   }, 2000);
               }
          }
      }); 
    }
});

});