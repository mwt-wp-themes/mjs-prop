var big_image, navbar_initialized, nowuiKit, $navbar, scroll_distance, oVal, transparent = !0,
    transparentDemo = !0,
    fixedTop = !1,
    backgroundOrange = !1,
    toggle_initialized = !1;

function debounce(a, o, r) {
    var l;
    return function() {
        var e = this,
            t = arguments;
        clearTimeout(l), l = setTimeout(function() {
            l = null, r || a.apply(e, t)
        }, o), r && !l && a.apply(e, t)
    }
}
jQuery(document).ready(function($) {
    jQuery('[data-toggle="tooltip"], [rel="tooltip"]').tooltip(), 0 != jQuery(".selectpicker").length && jQuery(".selectpicker").selectpicker({
        iconBase: "now-ui-icons",
        tickIcon: "ui-1_check"
    }), 768 <= jQuery(window).width() && 0 != (big_image = jQuery('.header[data-parallax="true"]')).length && jQuery(window).on("scroll", nowuiKit.checkScrollForParallax), jQuery('[data-toggle="popover"]').each(function() {
        color_class = jQuery(this).data("color"), jQuery(this).popover({
            template: '<div class="popover popover-' + color_class + '" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        })
    });
    var e = jQuery(".tagsinput").data("color");
    0 != jQuery(".tagsinput").length && jQuery(".tagsinput").tagsinput(), jQuery(".bootstrap-tagsinput").addClass("badge-" + e), nowuiKit.initNavbarImage(), jQuery(".navbar-collapse").addClass("show"), $navbar = jQuery(".navbar[color-on-scroll]"), scroll_distance = $navbar.attr("color-on-scroll") || 500, 0 != jQuery(".navbar[color-on-scroll]").length && (nowuiKit.checkScrollForTransparentNavbar(), jQuery(window).on("scroll", nowuiKit.checkScrollForTransparentNavbar)), jQuery(".form-control").on("focus", function() {
        jQuery(this).parent(".input-group").addClass("input-group-focus")
    }).on("blur", function() {
        jQuery(this).parent(".input-group").removeClass("input-group-focus")
    }), jQuery(".bootstrap-switch").each(function() {
        $this = jQuery(this), data_on_label = $this.data("on-label") || "", data_off_label = $this.data("off-label") || "", $this.bootstrapSwitch({
            onText: data_on_label,
            offText: data_off_label
        })
    }), 992 <= jQuery(window).width() && (big_image = jQuery('.page-header-image[data-parallax="true"]'), jQuery(window).on("scroll", nowuiKit.checkScrollForParallax)), jQuery(".carousel").carousel({
        interval: 4e3
    }), 0 != jQuery(".datetimepicker").length && jQuery(".datetimepicker").datetimepicker({
        icons: {
            time: "now-ui-icons tech_watch-time",
            date: "now-ui-icons ui-1_calendar-60",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: "now-ui-icons arrows-1_minimal-left",
            next: "now-ui-icons arrows-1_minimal-right",
            today: "fa fa-screenshot",
            clear: "fa fa-trash",
            close: "fa fa-remove"
        }
    }), 0 != jQuery(".datepicker").length && jQuery(".datepicker").datetimepicker({
        format: "MM/DD/YYYY",
        icons: {
            time: "now-ui-icons tech_watch-time",
            date: "now-ui-icons ui-1_calendar-60",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: "now-ui-icons arrows-1_minimal-left",
            next: "now-ui-icons arrows-1_minimal-right",
            today: "fa fa-screenshot",
            clear: "fa fa-trash",
            close: "fa fa-remove"
        }
    }), 0 != jQuery(".timepicker").length && jQuery(".timepicker").datetimepicker({
        format: "h:mm A",
        icons: {
            time: "now-ui-icons tech_watch-time",
            date: "now-ui-icons ui-1_calendar-60",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: "now-ui-icons arrows-1_minimal-left",
            next: "now-ui-icons arrows-1_minimal-right",
            today: "fa fa-screenshot",
            clear: "fa fa-trash",
            close: "fa fa-remove"
        }
    })
}), jQuery(window).on("resize", function($) {
    nowuiKit.initNavbarImage()
}), jQuery(document).on("click", ".navbar-toggler", function($) {
    $toggle = jQuery(this), 1 == nowuiKit.misc.navbar_menu_visible ? (jQuery("html").removeClass("nav-open"), nowuiKit.misc.navbar_menu_visible = 0, jQuery("#bodyClick").remove(), setTimeout(function() {
        $toggle.removeClass("toggled")
    }, 550)) : (setTimeout(function() {
        $toggle.addClass("toggled")
    }, 580), div = '<div id="bodyClick"></div>', jQuery(div).appendTo("body").click(function() {
        jQuery("html").removeClass("nav-open"), nowuiKit.misc.navbar_menu_visible = 0, setTimeout(function() {
            $toggle.removeClass("toggled"), jQuery("#bodyClick").remove()
        }, 550)
    }), jQuery("html").addClass("nav-open"), nowuiKit.misc.navbar_menu_visible = 1)
}), nowuiKit = {
    misc: {
        navbar_menu_visible: 0
    },
    checkScrollForTransparentNavbar: debounce(function() {
        jQuery(document).scrollTop() > scroll_distance ? transparent && (transparent = !1, jQuery(".navbar[color-on-scroll]").removeClass("navbar-transparent")) : transparent || (transparent = !0, jQuery(".navbar[color-on-scroll]").addClass("navbar-transparent"))
    }, 17),
    initNavbarImage: function() {
        var e = jQuery(".navbar").find(".navbar-translate").siblings(".navbar-collapse"),
            t = e.data("nav-image");
        null != t && (jQuery(window).width() < 991 || jQuery("body").hasClass("burger-menu") ? e.css("background", "url('" + t + "')").removeAttr("data-nav-image").css("background-size", "cover").addClass("has-image") : e.css("background", "").attr("data-nav-image", "" + t).css("background-size", "").removeClass("has-image"))
    },
    initSliders: function() {
        var e = document.getElementById("sliderRegular");
        noUiSlider.create(e, {
            start: 40,
            connect: [!0, !1],
            range: {
                min: 0,
                max: 100
            }
        });
        var t = document.getElementById("sliderDouble");
        noUiSlider.create(t, {
            start: [20, 60],
            connect: !0,
            range: {
                min: 0,
                max: 100
            }
        })
    },
    checkScrollForParallax: debounce(function() {
        oVal = jQuery(window).scrollTop() / 3, big_image.css({
            transform: "translate3d(0," + oVal + "px,0)",
            "-webkit-transform": "translate3d(0," + oVal + "px,0)",
            "-ms-transform": "translate3d(0," + oVal + "px,0)",
            "-o-transform": "translate3d(0," + oVal + "px,0)"
        })
    }, 6),
    initContactUsMap: function() {
        var e = new google.maps.LatLng(40.748817, -73.985428),
            t = {
                zoom: 13,
                center: e,
                scrollwheel: !1,
                styles: [{
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        color: "#e9e9e9"
                    }, {
                        lightness: 17
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "geometry",
                    stylers: [{
                        color: "#f5f5f5"
                    }, {
                        lightness: 20
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 17
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 29
                    }, {
                        weight: .2
                    }]
                }, {
                    featureType: "road.arterial",
                    elementType: "geometry",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 18
                    }]
                }, {
                    featureType: "road.local",
                    elementType: "geometry",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 16
                    }]
                }, {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [{
                        color: "#f5f5f5"
                    }, {
                        lightness: 21
                    }]
                }, {
                    featureType: "poi.park",
                    elementType: "geometry",
                    stylers: [{
                        color: "#dedede"
                    }, {
                        lightness: 21
                    }]
                }, {
                    elementType: "labels.text.stroke",
                    stylers: [{
                        visibility: "on"
                    }, {
                        color: "#ffffff"
                    }, {
                        lightness: 16
                    }]
                }, {
                    elementType: "labels.text.fill",
                    stylers: [{
                        saturation: 36
                    }, {
                        color: "#333333"
                    }, {
                        lightness: 40
                    }]
                }, {
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "transit",
                    elementType: "geometry",
                    stylers: [{
                        color: "#f2f2f2"
                    }, {
                        lightness: 19
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#fefefe"
                    }, {
                        lightness: 20
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#fefefe"
                    }, {
                        lightness: 17
                    }, {
                        weight: 1.2
                    }]
                }]
            },
            a = new google.maps.Map(document.getElementById("contactUsMap"), t);
        new google.maps.Marker({
            position: e,
            title: "Hello World!"
        }).setMap(a)
    },
    initContactUs2Map: function() {
        var e = new google.maps.LatLng(40.748817, -73.985428),
            t = {
                zoom: 13,
                center: e,
                scrollwheel: !1,
                styles: [{
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        color: "#e9e9e9"
                    }, {
                        lightness: 17
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "geometry",
                    stylers: [{
                        color: "#f5f5f5"
                    }, {
                        lightness: 20
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 17
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 29
                    }, {
                        weight: .2
                    }]
                }, {
                    featureType: "road.arterial",
                    elementType: "geometry",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 18
                    }]
                }, {
                    featureType: "road.local",
                    elementType: "geometry",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        lightness: 16
                    }]
                }, {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [{
                        color: "#f5f5f5"
                    }, {
                        lightness: 21
                    }]
                }, {
                    featureType: "poi.park",
                    elementType: "geometry",
                    stylers: [{
                        color: "#dedede"
                    }, {
                        lightness: 21
                    }]
                }, {
                    elementType: "labels.text.stroke",
                    stylers: [{
                        visibility: "on"
                    }, {
                        color: "#ffffff"
                    }, {
                        lightness: 16
                    }]
                }, {
                    elementType: "labels.text.fill",
                    stylers: [{
                        saturation: 36
                    }, {
                        color: "#333333"
                    }, {
                        lightness: 40
                    }]
                }, {
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "transit",
                    elementType: "geometry",
                    stylers: [{
                        color: "#f2f2f2"
                    }, {
                        lightness: 19
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#fefefe"
                    }, {
                        lightness: 20
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#fefefe"
                    }, {
                        lightness: 17
                    }, {
                        weight: 1.2
                    }]
                }]
            },
            a = new google.maps.Map(document.getElementById("contactUs2Map"), t);
        new google.maps.Marker({
            position: e,
            title: "Hello World!"
        }).setMap(a)
    }
};