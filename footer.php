<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MJS Landing Page
 */
global $mwt_option;
?>

    <footer class="footer">
      <div class="container">
        <nav>
          <?php
          wp_nav_menu( array(
              'theme_location'  => 'menu-2',
              'menu_id'         => 'footer-menu',
              'container'       => 'ul'
          ) );
          ?>
        </nav>
        <div class="copyright site-info">
          <?php if( !empty( $mwt_option['copyright-text'] ) ) : ?>
            <?php echo $mwt_option['copyright-text']; ?>
          <?php else: ?>
            Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo home_url('/'); ?>" class="copyright-link"><?php bloginfo('name'); ?></a>. All Rights Reserved.
          <?php endif; ?>
        </div><!-- .site-info --> 
      </div>
    </footer>

	</div><!-- .wrapper -->

  <?php wp_footer(); ?>

</body>
</html>
