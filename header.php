<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MJS Landing Page
 */

global $mwt_option;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
	<?php wp_head(); ?>
  
  <script src="<?php echo get_template_directory_uri(); ?>/assets/plugins/wow-js/wow.min.js"></script>
  <script>new WOW().init();</script>

</head>
<body <?php body_class(''); ?>>
  
  <!--
  <div id="preloader"></div>
  -->

  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-white fixed-top navbar-transparent" color-on-scroll="300">
    <div class="container">
      <div class="navbar-translate">
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar bar1"></span>
        <span class="navbar-toggler-bar bar2"></span>
        <span class="navbar-toggler-bar bar3"></span>
        </button>
        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <div class="logo-big">
              <img src="<?php echo $mwt_option['logo']['url']; ?>" class="img-fluid" alt="<?php bloginfo( 'name' ); ?>" width="90">
            </div>
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        wp_nav_menu( array(
            'theme_location'  => 'menu-1',
            'menu_id'         => 'primary-menu',
            'depth'           => 2,
            'container'       => 'ul',
            'menu_class'      => 'navbar-nav ml-auto',
            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
            'walker'          => new WP_Bootstrap_Navwalker()
        ) );
        ?>
      </div>
    </div>
  </nav>
  
  <?php //if( !is_front_page() ) Mwt_Now_Ui_Kit::page_header(); ?>

	<div class="wrapper">
