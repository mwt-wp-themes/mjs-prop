<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Landing Page
 */

global $mwt_option;

get_header();
?>

	<div id="primary" class="content-area">
    
    <?php get_page_header('banner', array( 'title' => single_post_title( '', false ) )); ?>
    
		<main id="main" class="site-main main main-raised mt-5">


      <div class="container">

      <?php if ( have_posts() ) : ?>

        <div class="row">
          <main class="col-sm-12 col-md-9 ml-auto mr-auto">

            <?php
            /* Start the Loop */
            while ( have_posts() ) :
              the_post();

              /*
               * Include the Post-Type-specific template for the content.
               * If you want to override this in a child theme, then include a file
               * called content-___.php (where ___ is the Post Type name) and that will be used instead.
               */
              get_template_part( 'template-parts/content', 'archive' );

            endwhile;

            the_posts_navigation();

          else :

            get_template_part( 'template-parts/content', 'none' );

          endif;
          ?>

          </main><!-- #main -->

          <?php get_sidebar(); ?>

        </div>

      </div>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
