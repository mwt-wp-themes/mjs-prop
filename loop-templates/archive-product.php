<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-4'); ?>>
  
  <div class="card card-pricing card-plain">
    <div class="card-body">
      <h5 class="category"><?php the_title(); ?></h5>
      <div class="icon icon-danger">
        <i class="now-ui-icons tech_headphones"></i>
      </div>
      <h3 class="card-title"><small>$</small>10</h3>
      <ul>
        <li>1000 MB</li>
        <li>3 email</li>
        <li>5 Databases</li>
      </ul>
      <a href="#pablo" class="btn btn-danger btn-round">
      Get it Now
      </a>
    </div>
  </div>

</article><!-- #post-<?php the_ID(); ?> -->

