<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Company', 'mwt' ),
    'id'    => 'mwt-company-option',
    'icon'  => 'el el-home'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'About', 'rensya' ),
    'id'         => 'mwt-about-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'company-name',
            'type'     => 'text',
            'title'    => __( 'Nama Perusahaan', 'rensya' ),
        ),
        array(
            'id'       => 'company-description',
            'type'     => 'textarea',
            'title'    => __( 'Alamat', 'rensya' ),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Contact', 'rensya' ),
    'id'         => 'mwt-contacts-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'contact-address',
            'type'     => 'textarea',
            'title'    => __( 'Alamat', 'rensya' ),
        ),
        array(
            'id'       => 'contact-email',
            'type'     => 'text',
            'title'    => __( 'Email', 'rensya' ),
            'validate' => 'email',
        ),
        array(
            'id'       => 'contact-phone',
            'type'     => 'text',
            'title'    => __( 'Telepon', 'rensya' ),
        ),
        array(
            'id'       => 'contact-fax',
            'type'     => 'text',
            'title'    => __( 'Fax', 'rensya' ),
        ),
        array(
            'id'       => 'contact-whatsapp',
            'type'     => 'text',
            'title'    => __( 'No. Whatsapp', 'rensya' ),
        ),
        array(
            'id'       => 'contact-livechat',
            'type'     => 'text',
            'title'    => __( 'Livechat URL', 'rensya' ),
        ),
    )
) );