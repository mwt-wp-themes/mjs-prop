<?php

Redux::setSection( $opt_name, array(
    'title'      => __( 'Custom Code', 'rensya' ),
    'desc'       => __( 'Custom Code', 'rensya' ),
    'id'         => 'mwt-custom-codes-options',
    'icon'       => 'el el-cogs',
    'fields'     => array(
        array(
            'id'       => 'custom-css',
            'type'     => 'ace_editor',
            'title'    => __('Custom CSS', 'rensya'),
            'subtitle' => __('Paste your CSS code here.', 'rensya'),
            'mode'     => 'css',
            'theme'    => 'monokai',
            'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
            'default'  => ""
        ),
        array(
            'id'       => 'custom-js',
            'type'     => 'ace_editor',
            'title'    => __('Custom JS', 'redux-framework-demo'),
            'subtitle' => __('Paste your javascript code here.', 'rensya'),
            'mode'     => 'javascript',
            'theme'    => 'chrome',
            'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
            'default'  => ""
        ),
    )
) );