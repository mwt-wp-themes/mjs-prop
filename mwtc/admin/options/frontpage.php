<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Homepage', 'mwt' ),
    'id'    => 'mwt-homepage-option',
    'desc'  => __( 'Basic fields as subsections.', 'mwt' ),
    'icon'  => 'el el-home'
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Hero', 'mwt' ),
    'id'     => 'mwt-hero-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'hero-type',
            'type'     => 'select',
            'title'    => __('Hero Type', 'mwt'), 
            'options'  => array(
                'banner' => __('Banner', 'mwt'), 
                'slider' => __('Slider', 'mwt'), 
                'video' => __('Video', 'mwt'), 
            ),
            'default' => 'banner'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( '- Hero Banner', 'mwt' ),
    'desc'  => __( 'Hero Banner Section', 'mwt' ),
    'id'     => 'mwt-hero-banner-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'hero-banner-img',
            'type'     => 'media',
            'title'    => __( 'Banner Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'hero-banner-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'mwt' ),
        ),
        array(
            'id'       => 'hero-banner-subtitle',
            'type'     => 'text',
            'title'    => __( 'Subtitle', 'mwt' ),
        ),
        array(
            'id'       => 'hero-banner-description',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'mwt' ),
        ),
        array(
            'id'       => 'hero-banner-size',
            'type'     => 'select',
            'title'    => __( 'Size', 'mwt' ),
            'options'  => array(
                'large' => __('Large (Fullscreen)', 'mwt'), 
                'small' => __('Medium', 'mwt'), 
                'xs' => __('Small', 'mwt'), 
            ),
            'default' => 'center'
        ),
        array(
            'id'       => 'hero-banner-alignment',
            'type'     => 'select',
            'title'    => __( 'Alignment', 'mwt' ),
            'options'  => array(
                'center' => __('Center', 'mwt'), 
                'left' => __('Left', 'mwt'), 
                'right' => __('Right', 'mwt'), 
            ),
            'default' => 'center'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( '- Hero Video', 'mwt' ),
    'desc'  => __( 'Video Section', 'mwt' ),
    'id'     => 'mwt-hero-video-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'hero-video-url',
            'type'     => 'text',
            'title'    => __( 'Video URL', 'mwt' ),
        ),
        array(
            'id'       => 'hero-video-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'mwt' ),
        ),
        array(
            'id'       => 'hero-video-subtitle',
            'type'     => 'text',
            'title'    => __( 'Subtitle', 'mwt' ),
        ),
        array(
            'id'       => 'hero-video-description',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'mwt' ),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( '- Hero Sliders', 'mwt' ),
    'desc'  => __( 'Slider/Banner Section', 'mwt' ),
    'id'     => 'mwt-hero-slider-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'          => 'hero-slides',
            'type'        => 'slides',
            'title'       => __('Slides Options', 'mwt'),
            'subtitle'    => __('Unlimited slides with drag and drop sortings.', 'mwt'),
            'placeholder' => array(
                'title'           => __('This is a title', 'mwt'),
                'description'     => __('Description Here', 'mwt'),
                'url'             => __('Give us a link!', 'mwt'),
            ),
        ),
        array(
            'id'       => 'hero-slide-alignment',
            'type'     => 'radio',
            'title'    => __('Slider Alignment', 'mwt'), 
            'options'  => array(
                'left' => __('Left', 'mwt'), 
                'center' => __('Center', 'mwt'), 
                'right' => __('Right', 'mwt'), 
            ),
            'default' => 'center'
        ),
        array(
            'id'       => 'hero-slide-button-text',
            'type'     => 'text',
            'title'    => __('Button Text', 'mwt'), 
            'default' => 'Read More'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Feature/Service', 'mwt' ),
    'id'     => 'mwt-features-section-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'feature-top-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
          'id'          => 'feature-top-image',
          'type'        => 'media',
          'url'         => true,
          'title'       => __('Image', 'mwt'),
        ),
        array(
            'id'       => 'feature-top-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
            'id'       => 'feature-top-subtitle',
            'type'     => 'text',
            'title'    => __('Subtitle', 'mwt'),
        ),
        array(
            'id'       => 'feature-top-description',
            'type'     => 'textarea',
            'title'    => __('Description', 'mwt'),
        ),
        array(
            'id'       => 'feature-top-content',
            'type'     => 'editor',
            'title'    => __('Contents', 'mwt'),
            'args'     => array(
              'teeny'   => true,
              'wpautop' => false
            )
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Portfolio', 'mwt' ),
    'id'     => 'mwt-portfolio-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'portfolio-enabled',
            'type'     => 'radio',
            'title'    => __('Enable Section', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '1'
        ),
        array(
            'id'       => 'portfolio-bg-image',
            'type'     => 'media',
            'title'    => __( 'Background Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'portfolio-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'portfolio-subtitle',
            'type'     => 'text',
            'title'    => __('Subtitle', 'mwt'), 
            'default' => ''
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Form', 'mwt' ),
    'id'     => 'mwt-contact-us-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'enable_form',
            'type'     => 'radio',
            'title'    => __('Enable Form', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'form_bg_image',
            'type'     => 'media',
            'title'    => __( 'Banner Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'form_title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'form_usia_min',
            'type'     => 'text',
            'validate' => 'numeric',
            'title'    => __('Usia Min', 'mwt'), 
            'default' => '17'
        ),
        array(
            'id'       => 'form_usia_max',
            'type'     => 'text',
            'validate' => 'numeric',
            'title'    => __('Usia Max', 'mwt'), 
            'default' => '29'
        ),
        array(
            'id'       => 'form_success_message',
            'type'     => 'textarea',
            'title'    => __('Submission Message', 'mwt'), 
            'desc'     => __('Pesan setelah visitor men-submit form.', 'mwt'),
            'default' => 'Sukses!'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: FAQ', 'mwt' ),
    'desc'  => __( 'Lihat semua FAQ di halaman <a href="edit.php?post_type=faqs">FAQs</a>. Untuk menambahkan faq, silahkan ke halaman <a href="post-new.php?post_type=faqs">tambah faqs</a>.', 'mwt' ),
    'id'     => 'mwt-faqs-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'faqs-enabled',
            'type'     => 'radio',
            'title'    => __('Enable FAQs', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'faqs_title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => 'FAQ'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Blog', 'mwt' ),
    'desc'  => __( 'Blog Section', 'mwt' ),
    'id'     => 'mwt-recent-blog-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'blog-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => 'Latest News'
        ),
        array(
            'id'       => 'blog-subtitle',
            'type'     => 'text',
            'title'    => __('Subtitle', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'blog-numberposts',
            'type'     => 'text',
            'title'    => __('Number of post to show', 'mwt'), 
            'default' => 2,
            'validate'  => 'numeric'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Testimonial', 'mwt' ),
    'id'     => 'mwt-testimonial-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'testimonial-enabled',
            'type'     => 'radio',
            'title'    => __('Enable Section', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '1'
        ),
        array(
            'id'       => 'testimonial-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => 'Customer Feedback'
        ),
        array(
            'id'       => 'testimonial-subtitle',
            'type'     => 'text',
            'title'    => __('Subtitle', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'testimonial-numberposts',
            'type'     => 'text',
            'title'    => __('Number of post to show', 'mwt'), 
            'default' => 2,
            'validate'  => 'numeric'
        ),
    )
) );