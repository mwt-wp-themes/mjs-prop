<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Header', 'mwt' ),
    'id'     => 'mwt-header-options',
    'icon'   => 'el el-website',
    'fields' => array(
        array(
            'id'       => 'slider-bg-image',
            'type'     => 'media',
            'title'    => __( 'Default Header Background', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'global-announcement',
            'type'     => 'textarea',
            'title'    => __( 'Global Announcement', 'mwt' ),
        ),
        array(
            'id'       => 'global-announcement-link',
            'type'     => 'text',
            'title'    => __( 'Global Announcement Link', 'mwt' ),
        ),
    )
) );