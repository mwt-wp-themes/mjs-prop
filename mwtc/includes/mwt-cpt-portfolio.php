<?php

// Register Custom Post Type
function portfolio_post_type() {

	$labels = array(
		'name'                  => _x( 'Portfolios', 'Post Type General Name', 'mjslp' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'mjslp' ),
		'menu_name'             => __( 'Portfolios', 'mjslp' ),
		'name_admin_bar'        => __( 'Portfolio', 'mjslp' ),
		'archives'              => __( 'Item Archives', 'mjslp' ),
		'attributes'            => __( 'Item Attributes', 'mjslp' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mjslp' ),
		'all_items'             => __( 'All Items', 'mjslp' ),
		'add_new_item'          => __( 'Add New Item', 'mjslp' ),
		'add_new'               => __( 'Add New', 'mjslp' ),
		'new_item'              => __( 'New Item', 'mjslp' ),
		'edit_item'             => __( 'Edit Item', 'mjslp' ),
		'update_item'           => __( 'Update Item', 'mjslp' ),
		'view_item'             => __( 'View Item', 'mjslp' ),
		'view_items'            => __( 'View Items', 'mjslp' ),
		'search_items'          => __( 'Search Item', 'mjslp' ),
		'not_found'             => __( 'Not found', 'mjslp' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mjslp' ),
		'featured_image'        => __( 'Featured Image', 'mjslp' ),
		'set_featured_image'    => __( 'Set featured image', 'mjslp' ),
		'remove_featured_image' => __( 'Remove featured image', 'mjslp' ),
		'use_featured_image'    => __( 'Use as featured image', 'mjslp' ),
		'insert_into_item'      => __( 'Insert into item', 'mjslp' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mjslp' ),
		'items_list'            => __( 'Items list', 'mjslp' ),
		'items_list_navigation' => __( 'Items list navigation', 'mjslp' ),
		'filter_items_list'     => __( 'Filter items list', 'mjslp' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'mjslp' ),
		'description'           => __( 'Portfolio Description', 'mjslp' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'portfolio_category' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'portfolio', $args );

}
add_action( 'init', 'portfolio_post_type', 0 );

// Register Custom Taxonomy
function portfolio_category_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'mjslp' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'mjslp' ),
		'menu_name'                  => __( 'Categories', 'mjslp' ),
		'all_items'                  => __( 'All Categories', 'mjslp' ),
		'parent_item'                => __( 'Parent Item', 'mjslp' ),
		'parent_item_colon'          => __( 'Parent Item:', 'mjslp' ),
		'new_item_name'              => __( 'New Item Name', 'mjslp' ),
		'add_new_item'               => __( 'Add New Item', 'mjslp' ),
		'edit_item'                  => __( 'Edit Item', 'mjslp' ),
		'update_item'                => __( 'Update Item', 'mjslp' ),
		'view_item'                  => __( 'View Item', 'mjslp' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'mjslp' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'mjslp' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'mjslp' ),
		'popular_items'              => __( 'Popular Items', 'mjslp' ),
		'search_items'               => __( 'Search Items', 'mjslp' ),
		'not_found'                  => __( 'Not Found', 'mjslp' ),
		'no_terms'                   => __( 'No items', 'mjslp' ),
		'items_list'                 => __( 'Items list', 'mjslp' ),
		'items_list_navigation'      => __( 'Items list navigation', 'mjslp' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

}
add_action( 'init', 'portfolio_category_taxonomy', 0 );