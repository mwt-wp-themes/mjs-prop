<?php

add_action( 'wp_ajax_mwt_get_data_mitra', 'mwt_get_data_mitra' );
add_action( 'wp_ajax_nopriv_mwt_get_data_mitra', 'mwt_get_data_mitra' );
function mwt_get_data_mitra() {
  $result = [];
  // WP_User_Query arguments
  $args = array(
    'role'          => 'Mitra',
  );

  // The User Query
  $user_query = new WP_User_Query( $args );

  // The User Loop
  if ( ! empty( $user_query->results ) ) {
    foreach ( $user_query->results as $user ) {
      $staf = get_user_by( 'ID', intval( get_user_meta( $user->ID, '_staf', true ) ) );
      $result[] = array(
          'id'          => $user->ID,
          'tanggal'     => $user->user_registered,
          'nama'        => $user->display_name,
          'email'       => $user->user_email,
          'hp'          => get_user_meta( $user->ID, '_nomorhp', true ),
          'staf'        => ( is_object( $staf ) ) ? $staf->display_name : '-',
      );
      
    }
  }
  
	echo json_encode( [ 'data' => $result ] );
	wp_die(); 
  
}

add_action( 'wp_ajax_mwt_update_data_mitra', 'mwt_update_data_mitra' );
add_action( 'wp_ajax_nopriv_mwt_update_data_mitra', 'mwt_update_data_mitra' );
function mwt_update_data_mitra() {
  $result = [];
  // WP_User_Query arguments
  $args = array(
    'role'           => 'Subscriber',
  );

  $user_id = $_POST['id'];
  $display_name = $_POST['nama'];
  $nomorhp = $_POST['hp'];

  $user_id = wp_update_user( array( 
    'ID'            => $user_id, 
    'display_name'  => $display_name,
    'first_name'    => $display_name
  ) );

  if ( is_wp_error( $user_id ) ) {
    // There was an error, probably that user doesn't exist.
  } else {
    // Success!
    update_user_meta( $user_id, '_nomorhp', $nomorhp );
  }	
  
	echo json_encode( $result );
	wp_die(); 
  
}

add_action( 'wp_ajax_mwt_get_data_jamaah', 'mwt_get_data_jamaah' );
add_action( 'wp_ajax_nopriv_mwt_get_data_jamaah', 'mwt_get_data_jamaah' );
function mwt_get_data_jamaah() {
  global $mwt;
  $result = [];
  
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'participants' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => -1,
//     'meta_name'              => '_status_verifikasi',
//     'meta_value'             => 'verified',
//     'meta_compare'           => '='
  );

  // The User Query
  $posts = get_posts( $args );

  foreach ( $posts as $post ) {
    
    $staf = Mwt::get_field( 'staf', $post->ID );
    $staf = ( !empty( $staf ) && is_object( $staf ) ) ? $staf->display_name : '';
    
    $mitra = Mwt::get_field( 'mitra', $post->ID );
    $mitra = ( !empty( $mitra ) && is_object( $mitra ) ) ? $mitra->display_name : '';
    
    $result[] = array(
        'id'          => $post->ID,
        'tanggal'     => $post->post_date,
        'nama'        => $post->post_title,
        'kavling'     => Mwt::get_field( 'kavling', $post->ID ),
        'company'     => ( !empty( Mwt::get_field( 'company', $post->ID ) ) ) ? Mwt::get_field( 'company', $post->ID ) : '',
        'email'       => Mwt::get_field( 'email', $post->ID ),
        'hp'          => Mwt::get_field( 'nomor_hp', $post->ID ),
        'staf'        => $staf,
        'mitra'        => $mitra,
    );

  }
  
	echo json_encode( [ 'data' => $result ] );
	wp_die(); 
  
}

add_action( 'wp_ajax_mwt_hapus_jamaah', 'mwt_hapus_jamaah' );
add_action( 'wp_ajax_nopriv_mwt_hapus_jamaah', 'mwt_hapus_jamaah' );
function mwt_hapus_jamaah() {
	$post_id = $_POST['id'];
  wp_delete_post( $post_id, true );
	wp_die();
}

add_action( 'wp_ajax_mwt_tambah_peminat_action', 'mwt_tambah_peminat_action' );
add_action( 'wp_ajax_nopriv_mwt_tambah_peminat_action', 'mwt_tambah_peminat_action' );
function mwt_tambah_peminat_action() {
  global $mwt, $mwt_option;
  $result = array();
  $nonce = $_POST['security'];
  if ( ! wp_verify_nonce( $nonce, 'mwt-nonce' ) ) {
      // This nonce is not valid.
      die( 'Security check' ); 
  } else {
    
    $data = array(
      'nama'      => $_POST['nama'],
      'telp'      => $_POST['telp'],
      'email'     => $_POST['email'],
      'kavling'   => $_POST['kavling']
    );

    // assign participant to staf
    $assignment = array();
    $user_query = new WP_User_Query( array( 'role__in' => [ 'Staf CS', 'staf_cs' ] ) );
    foreach ( $user_query->get_results() as $staf ) {
      $participants = 0;
      $args = array( 
        'post_type'           => array( 'participants' ),
        'post_status'         => array( 'publish' ),
        'nopaging'            => true,
        'posts_per_page'      => -1,
        'meta_query'          => array(
           'relation'   => 'AND',
           array(
             'key'      => 'staf',
             'value'    => $staf->ID,
             'type'     => 'NUMERIC',
             'compare'  => '='
           ),
        ), 
      );
      $posts = get_posts( $args );
      $assignment[$staf->ID] = count($posts); //( $participants+count($posts) );
    }
  
    if( count( $assignment ) > 0 ) {
      asort($assignment);
      $staf_assign = array_keys($assignment);
      $staf_id = $staf_assign[0];
    }
  
    $name = $data['nama'];
    $phone = $data['telp'];
    $email = $data['email'];
    $kavling = $data['kavling'];

    $args = array(
      'post_title'    => $name,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_type'     => 'participants'
    );
    $post_id = wp_insert_post( $args );

    update_field( 'email', $email, $post_id );
    update_field( 'nomor_hp', $phone, $post_id );
    update_field( 'kavling', $kavling, $post_id );
    update_field( 'mitra', $mwt->mitra['id'], $post_id );
    
    $result['submit_id'] = $post_id;
  
    if( isset( $staf_id ) ) {
      update_field( 'staf', $staf_id, $post_id );
    }
    
    // sms verifikasi
    $kode_unik = str_pad( substr( mt_rand( 100000, time() ), 0, 6 ), 3, 0, STR_PAD_LEFT );
    update_post_meta( $post_id, '_status_verifikasi', '' );
    update_post_meta( $post_id, '_sms_kode_unik', $kode_unik );
    
    //kirim sms    
    $mwt_wa = new Mwt_Wa();
    $sc = array(
      'kode_verifikasi'     => $kode_unik,
      'nama'                => $name,
      'kavling'             => $kavling,
      'staf_id'             => ( isset( $staf_id ) ) ? $staf_id : 0,
      'mitra_id'            => $mwt->mitra['id'],
    );
    $mwt_wa->send_staf_notification( array(
      'nomor' => $phone,
    ), $sc );
    
  }

  echo json_encode( $result );
  wp_die();
}

/**
 * Verifikasi SMS Action
 */
add_action( 'wp_ajax_mwt_verifikasi_sms_action', 'mwt_verifikasi_sms_action' );
add_action( 'wp_ajax_nopriv_mwt_verifikasi_sms_action', 'mwt_verifikasi_sms_action' );
function mwt_verifikasi_sms_action() {
  global $mwt, $mwt_option;
  $result = '';
  $result['error'] = 1;
  if( !empty( $_POST['submit_id'] ) ) {
    $post_id = intval( $_POST['submit_id'] );
    $kode_unik = $_POST['kode_verifikasi'];
    
    if( $kode_unik == get_post_meta( $post_id, '_sms_kode_unik', true ) ) {
      update_post_meta( $post_id, '_status_verifikasi', 'verified' ); 
      $result = array(
        'error' => 0,
        'msg'   => 'Terima kasih! Staf kami akan segera menghubungi anda.'
      );
      
      $staf = Mwt::get_field( 'staf', $post_id );
      if( !empty( $staf ) ) {
        $to = $staf->user_email;
        $subject = 'New Assignment';
        $body = '
          <p>Halo ' . $staf->display_name . '</p>
          <br>
          <p>Anda mendapatkan 1 customer untuk di follow up. Silahkan login ke akun anda untuk mengelola customer.</p>
          <br>
          <p>Terima kasih</p>
        ';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $headers[] = 'From: ' . get_bloginfo('name') . ' <'.get_bloginfo('admin_email').'>';
        $headers[] = 'Cc: '.get_bloginfo('admin_email');

        wp_mail( $to, $subject, $body, $headers );
      }
    } else {
        $result['msg'] = 'Kode verifikasi salah!';
    }

  } else {
    $result['msg'] = 'Data tidak ditemukan!';
  }
  
	echo json_encode( $result );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_followup_result', 'mwt_update_followup_result' );
add_action( 'wp_ajax_nopriv_mwt_update_followup_result', 'mwt_update_followup_result' );
function mwt_update_followup_result() {
  global $mwt, $mwt_option;
  
  $result = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'follow_up_result', $result, $post_id );
  
  $post = get_post( $post_id );
  if( $post ) {
    if( !empty( get_field('spv') ) ) {
      $spv = get_field('spv');
      $nomer_wa = get_user_meta( $spv->ID, '_nomorhp', true );
      if( !empty( $nomer_wa ) ) {
        $mwt_wa = new Mwt_Wa();
        $mwt_wa->send_request( array(
          'nomor' => $nomer_wa,
          'pesan' => $result
        )); 
      }
    } 
  }
  
	echo json_encode( $result );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_cust_status', 'mwt_update_cust_status' );
add_action( 'wp_ajax_nopriv_mwt_update_cust_status', 'mwt_update_cust_status' );
function mwt_update_cust_status() {
  global $mwt, $mwt_option;
  
  $status = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'status', $status, $post_id );
  
	echo json_encode( 1 );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_spv_followup_result', 'mwt_update_spv_followup_result' );
add_action( 'wp_ajax_nopriv_mwt_update_spv_followup_result', 'mwt_update_spv_followup_result' );
function mwt_update_spv_followup_result() {
  global $mwt, $mwt_option;
  
  $result = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'spv_notes', $result, $post_id );
  
	echo json_encode( $result );
	wp_die();
}

add_action( 'wp_ajax_mwt_assign_spv', 'mwt_assign_spv' );
add_action( 'wp_ajax_nopriv_mwt_assign_spv', 'mwt_assign_spv' );
function mwt_assign_spv() {
  
  $result = $_POST['value'];
  $post_id = $_POST['pk'];
  $spv = get_user_by( 'ID', $result );
  update_field( 'spv', $spv, $post_id );
  
	echo json_encode( $result );
	wp_die();
}