<?php

class Mwt  {
  
  private $prefix = "mwtc";
  
  public function __construct() {
    
    $this->load_dependencies();
    
    add_filter('show_admin_bar', '__return_false');
    
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

    if( is_admin() ) {
      
      add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );
      add_action( 'show_user_profile', array( $this, 'my_show_extra_profile_fields' ) );
      add_action( 'edit_user_profile', array( $this, 'my_show_extra_profile_fields' ) );
      add_action( 'personal_options_update', array( $this, 'my_save_extra_profile_fields' ) );
      add_action( 'edit_user_profile_update', array( $this, 'my_save_extra_profile_fields' ) );
    }
  }  
  
  public function load_dependencies() {
    
    if( session_id() == '' ) session_start();
    
    require_once $this->include_path( 'admin/admin-init.php' );
    require_once $this->include_path( 'mwt-now-ui-kit.php' );
    require_once $this->include_path( 'includes/mwt-cpt-portfolio.php' );
    require_once $this->include_path( 'includes/mwt-cpt-testimonial.php' );
    require_once $this->include_path( 'includes/mwt-cpt-info-contents.php' );
    require_once $this->include_path( 'mwt-helpers.php' );
    require_once $this->include_path( 'mwt-ajax.php' );
    
  }
  
  private function include_path( $file ) {
    return get_template_directory() . '/' . $this->prefix . '/' . ltrim( $file, '/' );
  }
  
  public function register_admin_scripts( $hook ) {
    if (  !in_array( $hook, ['mjs_page_mjs_mitra', 'mjs_page_mjs_customer' ] ) ) {
        return;
    }
    wp_enqueue_style( 'admin_pure_css', 'https://unpkg.com/purecss@1.0.0/build/pure-min.css' );
    wp_enqueue_style( 'admin_datatable_css', get_template_directory_uri() . '/assets/DataTables/datatables.min.css' );
    wp_enqueue_script( 'admin_datatable_script', get_template_directory_uri() . '/assets/DataTables/datatables.min.js', array('jquery') );
  }
  
  public function enqueue_scripts() {
    wp_enqueue_style( 'font-montserrat-css', 'https://fonts.googleapis.com/css?family=Montserrat:400,700,200|Open+Sans+Condensed:700' );
    wp_enqueue_style( 'font-awesome-css', 'https://use.fontawesome.com/releases/v5.0.6/css/all.css' );
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    //wp_enqueue_style( 'demo-css', get_template_directory_uri() . '/assets/demo/demo.css' );

    
    // extra styles
    wp_enqueue_style( 'jssocials-css', 'https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css' );
    wp_enqueue_style( 'jssocials-theme-css', 'https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-minima.css' );
    wp_enqueue_style( 'mwt-style', get_stylesheet_uri() );
    
    // Register scripts
    //wp_enqueue_script( 'jquery-js', get_stylesheet_directory_uri() . '/assets/js/core/jquery.min.js', array(), '', true ); 
    wp_enqueue_script( 'popper-js', get_stylesheet_directory_uri() . '/assets/js/core/popper.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/assets/js/core/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bootstrap-switch-js', get_stylesheet_directory_uri() . '/assets/js/plugins/bootstrap-switch.js', array('jquery'), '', true );
    wp_enqueue_script( 'nouislider-js', get_stylesheet_directory_uri() . '/assets/js/plugins/nouislider.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bootstrap-datepicker-js', get_stylesheet_directory_uri() . '/assets/js/plugins/bootstrap-datepicker.js', array('jquery'), '', true );
    //wp_enqueue_script( 'googlemaps-js', 'https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE', array('jquery'), '', true );
    
    // extra scripts
    wp_enqueue_script( 'loadingoverlay-scripts', 'https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js', array('jquery'), '', true ); 
    wp_enqueue_script( 'jssocials-scripts', 'https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js', array('jquery'), '', true ); 
    
    // localize
    wp_enqueue_script( 'app-scripts' );
    wp_register_script( 'app-scripts', get_stylesheet_directory_uri() . '/assets/js/app.js' );
    wp_localize_script( 'app-scripts', 'mjs_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'wp_nonce' => wp_create_nonce( 'mwt-nonce' ) ) );
    wp_enqueue_script( 'app-scripts' );
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }
    
    
    // dynamics
    $scripts = '';    
    if( basename( get_page_template_slug() ) == "template-account.php" ) {
      global $user_info;
      $user_role = implode(', ', $user_info->roles);
      $role = strtolower( str_replace( ' ', '_', $user_role ) );
      wp_enqueue_style( 'datatable-css', 'https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css' );
      wp_enqueue_style( 'jquery-editable-css', 'https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/css/jquery-editable.css' );
      wp_enqueue_script( 'jquery-poshytip-script', 'https://cdnjs.cloudflare.com/ajax/libs/poshytip/1.2/jquery.poshytip.min.js', array('jquery') );
      wp_enqueue_script( 'jquery-editable-script', 'https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/js/jquery-editable-poshytip.min.js', array('jquery') );
      wp_enqueue_script( 'datatable-script',  'https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js', array('jquery') );
      if ( $role == "staf_cs" || $role == "staf_mjs" ) {
        $scripts .= '
          jQuery(function(){
              jQuery(\'.supervisor-field\').editable({   
                  source: [
        ';
        $spv_query = new WP_User_Query( array( 'role__in' => [ 'staf_spv' ] ) );
        $counts = count( $spv_query->get_results() );
        $count = 1;
        foreach ( $spv_query->get_results() as $spv ) {
          $scripts .= '{value: '.$spv->ID.', text: "'.$spv->display_name.'"}';
          $scripts .= ( $count < $counts ) ? ',' : '';
          $count++;
        }
        $scripts .= '
                     ]
              });
          });
          
          jQuery(function($){
              $(".followup-result-input").editable({
                  url: "' . admin_url('admin-ajax.php') . '?action=mwt_update_followup_result",
                  title: "Enter comments",
                  rows: 10
              });
          });
          jQuery(function($){
              $(".cust-status").editable({
                  source: [
                        {value: "Pending", text: "Pending"},
                        {value: "Confirmed", text: "Confirmed"}
                     ],
                  success: function(response, newValue) {
                      if(!response.success) window.location.reload();
                  }
              });
          });
        ';
      } elseif ( $role == "staf_spv" ) {
        $scripts .= '
          jQuery(function($){
              $(".spv-followup-result-input").editable({
                  url: "' . admin_url('admin-ajax.php') . '?action=mwt_update_spv_followup_result",
                  title: "Enter comments",
                  rows: 10
              });
          });
        ';
      } elseif ( $role == "staf_manager" ) {
        $scripts .= '
          jQuery(function($){
              $(".komisi_mitra-input").editable({
                  url: "' . admin_url('admin-ajax.php') . '?action=mwt_update_komisi_mitra",
                  title: "Input komisi"
              });
          });
          jQuery(function($){
              $(".komisi-status").editable({
                  source: [
                        {value: "Pending", text: "Pending"},
                        {value: "Sudah dibayar", text: "Sudah dibayar"}
                     ]
              });
          });
        ';
      }
    }
    
    if( is_home() || is_front_page() ) {
      wp_enqueue_style( 'owl-carousel-css', get_stylesheet_directory_uri() . '/assets/plugins/owlcarousel/assets/owl.carousel.min.css' );
      wp_enqueue_style( 'owl-carousel-theme-css', get_stylesheet_directory_uri() . '/assets/plugins/owlcarousel/assets/owl.theme.default.min.css' );
      wp_enqueue_script( 'owl-carousel-scripts', get_stylesheet_directory_uri() . '/assets/plugins/owlcarousel/owl.carousel.min.js', array('jquery'), '', true ); 
      $scripts .= '
        jQuery("#gallery").owlCarousel({
            center: true,
            items:1,
            loop:true,
            margin:10,
            dots:false,
            autoplay:true,
            responsive:{
                600:{
                    items:3
                }
            }
        });
      ';
    }
    
//     wp_enqueue_script( 'now-ui-kit-js', get_template_directory_uri() . '/assets/js/now-ui-kit.js', array('jquery') );
    wp_enqueue_script( 'now-ui-kit-js', get_stylesheet_directory_uri() . '/assets/js/now-ui-kit.js', array('jquery'), '', true );
    wp_add_inline_script( 'now-ui-kit-js', trim($scripts) );
    
  }
  
  public static function get_field( $name, $post_id = '' ) {
    if( !function_exists( 'get_field' ) ) {
      return;
    }
    $post_id = ( '' != $post_id ) ? $post_id : get_the_ID();
    return get_field( $name, $post_id );
  }
  
  public static function dump( $var, $exit = false ) {
    echo "<pre>"; var_dump( $var ); echo "</pre>";
    if( $exit ) exit;
  }
  
  public function my_show_extra_profile_fields( $user ) { ?>
  <h3>Extra profile information</h3>
      <table class="form-table">
  <tr>
              <th><label for="phone">Nomor Whatsapp</label></th>
              <td>
              <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( '_nomorhp', $user->ID ) ); ?>" class="regular-text" /><br />
                  <span class="description">Please enter your phone number.</span>
              </td>
  </tr>
  </table>
  <?php }

  public function my_save_extra_profile_fields( $user_id ) {

  if ( !current_user_can( 'edit_user', $user_id ) )
      return false;

  update_usermeta( $user_id, '_nomorhp', $_POST['phone'] );
  }
  
}

$mwt = new Mwt();