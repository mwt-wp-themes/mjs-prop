<?php

function is_edit_page($new_edit = null){
    global $pagenow;
    //make sure we are on the backend
    if (!is_admin()) return false;


    if($new_edit == "edit")
        return in_array( $pagenow, array( 'post.php',  ) );
    elseif($new_edit == "new") //check for new post page
        return in_array( $pagenow, array( 'post-new.php' ) );
    else //check for either new or edit
        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
}

function mwt_currency( $amount, $prefix = '' ) {  
  $result = '';
  $result .= ( !empty( $prefix ) ) ? $prefix : '';
  $result .= number_format( $amount, 0, ',', '.' );
  return $result;
}

function user_update_profile_action() {
  if( isset( $_POST['submit'] ) ) {
    $first_name = esc_attr($_POST['first_name']);
    $nomorhp = $_POST['nomor_hp'];
    $nomortelp = $_POST['nomor_telp'];

    $user_id = wp_update_user( array( 
      'ID'            => get_current_user_id(),
      'first_name'    => $first_name, 
      'display_name'  => $first_name,
    ) );

    if ( is_wp_error( $user_id ) ) {
      // There was an error, probably that user doesn't exist.
    } else {
      update_user_meta( $user_id, '_nomorhp', $nomorhp );
      update_user_meta( $user_id, '_nomortelp', $nomortelp );
      
      if( !empty( $_POST['nama_bank'] ) ) {
        update_user_meta( $user_id, '_nama_bank', $_POST['nama_bank'] );
      }
      if( !empty( $_POST['nomor_rekening'] ) ) {
        update_user_meta( $user_id, '_norek', $_POST['nomor_rekening'] );
      }
      
      echo '<div class="alert alert-success">';
      echo 'Profil berhasil diperbarui';
      echo '</div>';
    }	 
  }
}

function mwt_pagination() {
  $args = array(
//     'base'               => '%_%',
//     'format'             => '?paged=%#%',
  // 	'total'              => 1,
  // 	'current'            => 0,
  // 	'show_all'           => false,
  // 	'end_size'           => 1,
  // 	'mid_size'           => 2,
  // 	'prev_next'          => true,
  // 	'prev_text'          => __('« Previous'),
//     'next_text'          => __('Next »'),
    'type'               => 'list',
//     'add_args'           => false,
//     'add_fragment'       => '',
//     'before_page_number' => '',
//     'after_page_number'  => ''
  );
  return paginate_links( $args );
  $links = paginate_links( $args );
  foreach( $links as $link ) {
//     var_dump($link);
    echo $link;
//     echo '<a href="https://salamtour.com/testimonial-jamaah-umroh-dan-haji-plus/8" class="baten" data-ci-pagination-page="3">3</a>';
  }
}

function mwt_pesan_paket_umroh_action() {
  global $mwt, $mwt_option;
  if( isset( $_POST['submit'] ) ) {

    // assign participant to staf
    $assignment = array();
    $user_query = new WP_User_Query( array( 'role__in' => [ 'staf_cs', 'staf_mjs' ] ) );
    foreach ( $user_query->get_results() as $staf ) {
      $participants = 0;
      $args = array( 
        'post_type'           => array( 'participants' ),
        'post_status'         => array( 'publish' ),
        'nopaging'            => true,
        'posts_per_page'      => -1,
        'meta_query'          => array(
           'relation'   => 'AND',
           array(
             'key'      => 'staf',
             'value'    => $staf->ID,
             'type'     => 'NUMERIC',
             'compare'  => '='
           ),
        ), 
      );
      $posts = get_posts( $args );
      $assignment[$staf->ID] = ( $participants+count($posts) );
    }
  
    if( count( $assignment ) > 0 ) {
      asort($assignment);
      $staf_assign = array_keys($assignment);
      $staf_id = $staf_assign[0];
    }
  
    $name = $_POST['nama'];
    $phone = $_POST['telepon'];
    $email = $_POST['email'];
    $paket_id = $_POST['paket_id'];

    $args = array(
      'post_title'    => $name,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_type'     => 'participants'
    );
    $post_id = wp_insert_post( $args );

    $paket = get_post( $paket_id );
    update_field( 'paket_umroh', $paket, $post_id );
    
    $mitra = get_user_by( 'id', $mwt->mitra['id'] );
    update_field( 'mitra', $mitra, $post_id );
    
    update_field( 'email', $email, $post_id );
    update_field( 'nomor_hp', $phone, $post_id );
    //update_field( 'follow_up_status', 'Pending', $post_id );
  
    if( isset( $staf_id ) ) {
      update_field( 'staf', $staf_id, $post_id );
      
      $staf = get_userdata( $staf_id );
      $to = $staf->user_email;
      $subject = 'New Assignment';
      $body = '
        <p>Halo ' . $staf->display_name . '</p>
        <p>Anda mendapatkan 1 customer untuk di follow up. Silahkan login ke akun anda untuk mengelola customer.</p>
        <p>Terima kasih</p>
      ';
      $headers = array('Content-Type: text/html; charset=UTF-8');
      $headers[] = 'From: ' . get_bloginfo('name') . ' <info@umrohbermakna.com>';
      $headers[] = 'Cc: info@umrohbermakna.com';

      wp_mail( $to, $subject, $body, $headers );
    }
    
    wp_redirect( get_permalink( $mwt_option['thankyou-page-id'] ) );
    
  }
}

function convertCurrency($amount,$from_currency,$to_currency){
  $apikey = '';

  $from_Currency = urlencode($from_currency);
  $to_Currency = urlencode($to_currency);
  $query =  "{$from_Currency}_{$to_Currency}";

  //$json = file_get_contents("https://api.currencyconverterapi.com/api/v6/convert?q={$query}&compact=ultra&apiKey={$apikey}");
  $json = file_get_contents("https://free.currencyconverterapi.com/api/v6/convert?q={$query}&compact=ultra&apiKey={$apikey}");
  $obj = json_decode($json, true);

  $val = floatval($obj["$query"]);


  $total = $val * $amount;
  return number_format($total, 2, '.', '');
}

function mwt_mitra_registration_form_action() {
    if ( isset($_POST['submit-mitra'] ) ) {
        registration_validation(
          $_POST['username'],
          $_POST['password'],
          $_POST['email'],
          $_POST['full_name'],
          $_POST['alamat'],
          $_POST['nomor_hp'],
          $_POST['nomor_wa']
        );
         
        // sanitize user form input
        global $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $full_name  =   sanitize_text_field( $_POST['full_name'] );
        $alamat     =   esc_textarea( $_POST['alamat'] );
        $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
        $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
          $username,
          $password,
          $email,
          $full_name,
          $alamat,
          $nomor_hp,
          $nomor_wa
        );
    }
}

function registration_validation( $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa )  {
  global $reg_errors;
  $reg_errors = new WP_Error;

  if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
      $reg_errors->add('field', 'Required form field is missing');
  }

  if ( 4 > strlen( $username ) ) {
      $reg_errors->add( 'username_length', 'Username too short. At least 4 characters is required' );
  }

  if ( username_exists( $username ) )
      $reg_errors->add('user_name', 'Username sudah digunakan!');

  if ( ! validate_username( $username ) ) {
      $reg_errors->add( 'username_invalid', 'Sorry, the username you entered is not valid' );
  }

  if ( 5 > strlen( $password ) ) {
      $reg_errors->add( 'password', 'Password length must be greater than 5' );
  }

  if ( !is_email( $email ) ) {
      $reg_errors->add( 'email_invalid', 'Email tidak valid!' );
  }

  if ( email_exists( $email ) ) {
      $reg_errors->add( 'email', 'Alamat email sudah digunakan!' );
  }

//   if ( ! empty( $website ) ) {
//       if ( ! filter_var( $website, FILTER_VALIDATE_URL ) ) {
//           $reg_errors->add( 'website', 'Website is not a valid URL' );
//       }
//   }

  if ( is_wp_error( $reg_errors ) ) {

      foreach ( $reg_errors->get_error_messages() as $error ) {

          echo '<div class="alert alert-error">';
          echo '<strong>ERROR</strong>:';
          echo $error . '<br/>';
          echo '</div>';
        
          break;

      }

  }
}

function complete_registration( $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa, $staf ) {
    global $reg_errors, $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa, $staf;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
          'user_login'    =>   $username,
          'user_email'    =>   $email,
          'user_pass'     =>   $password,
          'first_name'    =>   $full_name,
          'last_name'     =>   '',
          'nickname'      =>   $username,
          'role'          =>   'mitra',
        //'description'   =>   $bio,
        );
        $user_id = wp_insert_user( $userdata );
        if( $user_id ) {
          update_user_meta( $user_id, '_alamat', $alamat );
          update_user_meta( $user_id, '_nomorhp', $nomor_hp );
          update_user_meta( $user_id, '_nomortelp', $nomor_wa );
          update_user_meta( $user_id, '_staf', $staf );
        }
        echo '<div class="alert alert-success">';
        echo '<strong></strong>';
        echo 'Mitra berhasil ditambahkan.';
        echo '</div>';
    }
}

// Hijack the option, the role will follow!
add_filter('pre_option_default_role', function($default_role){
    // You can also add conditional tags here and return whatever
    return ( !empty( $_GET['role'] ) ) ? $_GET['role'] : $default_role;
});

function mwt_singkat_harga( $harga ) {
  $text = '';
  $rounded = floor( intval( $harga ) );
  $harga = mwt_currency($rounded);
  $array = explode( '.', $harga );
  
  if( strlen( $array[0] ) > 1 && count( $array ) >= 3 && count( $array ) < 6) {
    $harga = $array[0] . '.' . substr($array[1],0,1);
    if( count( $array ) == 5 ) {
      $text = 'T';
    } elseif( count( $array ) == 4 ) {
      $text = 'M';
    } elseif( count( $array ) == 3 ) {
      $text = 'Jt';
    } 
  }
  
  $harga = ( $harga != '' ) ? $harga . ' ' . $text : '';
  return $harga;
}

function mwt_add_every_five_minutes( $schedules ) {
	$schedules['every_five_minutes'] = array(
		'interval' => 300,
		'display' => __('Every Five Minutes')
	);
	return $schedules;
}
add_filter( 'cron_schedules', 'mwt_add_every_five_minutes' ); 

register_activation_hook(__FILE__, 'mwt_activation');
function mwt_activation() {
    if (! wp_next_scheduled ( 'mwt_every_five_minutes_event' )) {
  wp_schedule_event( time(), 'every_five_minutes', 'mwt_every_five_minutes_event' );
    }
}

add_action('mwt_every_five_minutes_event', 'do_this_every_five_minutes');
function do_this_every_five_minutes() {
  // do something every hour
  $args = array(
    'post_type'              => array( 'participants' ),
    'post_status'            => array( 'publish' ),
    'nopaging'               => true,
    'posts_per_page'         => -1,
    'meta_name'              => '_status_verifikasi',
    'meta_value'             => 'verified',
    'meta_compare'           => '!='
  );
  $event_posts = get_posts( $args );
  foreach( $event_posts as $event_post ) {
    wp_delete_post( $event_post->ID, true );
  }
}

register_deactivation_hook(__FILE__, 'mwt_deactivation');
function mwt_deactivation() {
  wp_clear_scheduled_hook('mwt_every_five_minutes_event');
}

function get_page_header($header_type = 'banner', $params = array()) {
  global $mwt_option;
  $header_size = ( !empty( Mwt::get_field('header_size') ) ) ? Mwt::get_field('header_size') : 'small';
  if( !empty( $params['title'] ) ) {
    $title = $params['title'];
  } else {
    $title = ( !empty( Mwt::get_field('custom_title') ) ) ? Mwt::get_field('custom_title') : get_the_title(); 
  }
  $subtitle = ( !empty( $params['subtitle'] ) ) ? strip_tags($params['subtitle']) : strip_tags(Mwt::get_field('subtitle'));
  
  $html = '';  
  $html .= '<header class="page-header page-header-' . $header_size . ' ' . $header_type . '">';
  
  $bg_image = get_template_directory_uri() . '/assets/img/bg32.jpg';
  
  if( !empty( $mwt_option['slider-bg-image']['url'] ) )
    $bg_image = $mwt_option['slider-bg-image']['url'];
  
  if( $header_type == 'banner' ) : 

    $html .= '<div class="page-header-image" data-parallax="true" style="background-image: url(\'' . $bg_image . '\');"></div>';
    $html .= '<div class="content-center">';
    $html .= '<div class="row">';
    $html .= '<div class="col-md-8 ml-auto mr-auto">';
    $html .= '<h1 class="title">' . $title . '</h1>';
    if( !empty( $subtitle ) )
      $html .= '<h4>' . $subtitle . '</h4>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
  
  elseif( $header_type == 'video' ) :
  
  else: 
  
  endif; 
  
  $html .= '</header>';
  
  echo $html;
  
}

// function mwt_add_every_five_minutes( $schedules ) {
// 	$schedules['every_five_minutes'] = array(
// 		'interval' => 300,
// 		'display' => __('Every Five Minutes')
// 	);
// 	return $schedules;
// }
// add_filter( 'cron_schedules', 'mwt_add_every_five_minutes' ); 

// register_activation_hook(__FILE__, 'mwt_activation');
// function mwt_activation() {
//     if (! wp_next_scheduled ( 'mwt_every_five_minutes_event' )) {
//   wp_schedule_event( time(), 'every_five_minutes', 'mwt_every_five_minutes_event' );
//     }
// }

// add_action('mwt_every_five_minutes_event', 'do_this_every_five_minutes');
// function do_this_every_five_minutes() {
//   // do something every hour
//   $args = array(
//     'post_type'              => array( 'participants' ),
//     'post_status'            => array( 'publish' ),
//     'nopaging'               => true,
//     'posts_per_page'         => -1,
//     'meta_name'              => '_status_verifikasi',
//     'meta_value'             => 'verified',
//     'meta_compare'           => '!='
//   );
//   $event_posts = get_posts( $args );
//   foreach( $event_posts as $event_post ) {
//     wp_delete_post( $event_post->ID, true );
//   }
// }

// register_deactivation_hook(__FILE__, 'mwt_deactivation');
// function mwt_deactivation() {
//   wp_clear_scheduled_hook('mwt_every_five_minutes_event');
// }
