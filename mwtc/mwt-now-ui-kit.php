<?php

/**
 * Required plugins:
 * - Advanced Custom Fields
 */

class Mwt_Now_Ui_Kit extends Mwt {
  
  
  public static function page_header($options = array()) {
    $opt = array_merge($options, array(
      'parallax'    => true,
      'class'       => 'page-header page-header-mini',
      'background'  => get_template_directory_uri() . '/assets/img/bg9.jpg'
    ));
    if( is_singular() && 'post' == get_post_type() && has_post_thumbnail() ) {
      $opt['class'] = 'page-header page-header-small';
      $opt['background'] = get_the_post_thumbnail_url( get_the_ID(), 'large' );
    }
    if( is_home() ) {
      $title = "MWT Blog";
    } else {
      $title = get_the_title();
    }
    $html = '';
    $html .= '<header class="' . $opt['class'] . '">';
    $html .= '	<div class="page-header-image" data-parallax="' . $opt['class'] . '" style="background-image: url(\'' . $opt['background'] . '\'); transform: translate3d(0px, 0px, 0px);"></div>';
    $html .= '<div class="content-center">';
    $html .= '
    		<div class="row">
        		<div class="col-md-8 ml-auto mr-auto text-center">
               <h2 class="title">' . $title . '</h2>
            </div>
        </div>        
    ';
    $html .= '</div></header>';
    
    if( is_singular() && 'post' == get_post_type() ) {
      $html .= '


<div class="section">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="button-container">
				<a href="#pablo" class="btn btn-primary btn-round btn-lg">
				<i class="now-ui-icons text_align-left"></i> Read Article
				</a>
				<a href="#pablo" class="btn btn-icon btn-lg btn-twitter btn-round">
				<i class="fab fa-twitter"></i>
				</a>
				<a href="#pablo" class="btn btn-icon btn-lg btn-facebook btn-round">
				<i class="fab fa-facebook-square"></i>
				</a>
				<a href="#pablo" class="btn btn-icon btn-lg btn-google btn-round">
				<i class="fab fa-google"></i>
				</a>
			</div>
		</div>
	</div>
</div>


      ';
    }
    
    echo $html;
  }
  
  
}