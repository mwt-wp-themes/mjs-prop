<?php

class Mwt_Sms_Notifikasi {
  
  public $userkey = "tovic24";
  public $passkey = "mitra7878";
  
  public function __construct() {
    $this->redux_option_init();
    add_action( 'wp_ajax_wa_callback', array( $this, 'wa_callback' ) );
    add_action( 'wp_ajax_nopriv_wa_callback', array( $this, 'wa_callback' ) );
  }
  
  public function redux_option_init() {
    if( class_exists('Redux') ) {
      Redux::setSection( 'mwt_option', array(
          'title'  => __( 'SMS', 'mwt' ),
          'id'     => 'mwt-sms-option',
          'icon'   => 'el el-message',
          'fields' => array(
              array(
                  'id'       => 'mwt-sms-content',
                  'type'     => 'textarea', 
                  'title'    => __('Konten SMS', 'mwt'),
                  'desc'     => __('Gunakan shortcode <code>[kode_verifikasi]</code> untuk digantikan dengan nomor acak kode verifikasi.', 'mwt'),
              ),
          )
      ) );
    }
  }
  
  public function send( $to, $message = '' ) {
    
    $ch = curl_init();
    $url = "http://reguler.sms-notifikasi.com/apps/smsapi.php";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "userkey=".$this->userkey."&passkey=".$this->passkey."&nohp=".$to."&pesan=".urlencode($message));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT,60);
    curl_setopt($ch, CURLOPT_POST, 1);
    $results = curl_exec($ch);
    curl_close($ch);

  }
  
}