<?php

class Mwt_Latest_News_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mwt_latest_news_widget', // Base ID
			esc_html__( 'SurgaTekno Latest News', 'understrap' ), // Name
			array( 'description' => esc_html__( '', 'understrap' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']; 
		global $mwt_option; 
    // WP_Query arguments
    $args = array(
      'post_type'              => array( 'post' ),
      'post_status'            => array( 'publish' ),
      'nopaging'               => false,
      'posts_per_page'         => 5,
      'order'                  => 'ASC',
      'orderby'                => 'date',
    );
    $featured = array();
    $news = array();
    $news_posts = get_posts( $args );
    $count = 1;
    foreach( $news_posts as $news_post ) {
      if( $count == 1 ) {
        $featured = $news_post;
      } else {
        $news[] = $news_post;
      }
      $count++;
    }
?>

      <section class="section section-news-carousel">
        <div class="row row-eq-height">
          <?php  ?>

          <div class="col-sm-12 col-md-6 col-lg-6 featcard">
            <div class="card bg-dark text-white">
              <img class="card-img img-fluid" src="<?php echo get_the_post_thumbnail_url( $featured->ID ); ?>" alt="">
              <div class="card-img-overlay d-flex linkfeat">
                <a href="<?php echo get_the_permalink( $featured->ID ); ?>" class="align-self-end">
<!--                             <span class="badge">Ekspor</span>  -->
                  <h4 class="card-title"><?php echo get_the_title( $featured->ID ); ?></h4>
                </a>
              </div>
            </div>
          </div>
          <div class="col-6 d-none d-lg-block">
            <div class="row">
                  <?php foreach( $news as $new ) : ?>
                  <div class="col-6">
                    <div class="card bg-dark text-white">
                      <img class="card-img img-fluid" src="<?php echo get_the_post_thumbnail_url( $new->ID ); ?>" alt="">
                      <div class="card-img-overlay d-flex linkfeat">
                        <a href="<?php echo get_the_permalink( $new->ID ); ?>" class="align-self-end">
      <!--                         <span class="badge">Finansial</span>  -->
                          <h6 class="card-title"><?php echo get_the_title( $new->ID ); ?></h6>
                        </a>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>
            </div>
          </div>
        </div>
      </section>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'understrap' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}

function register_mwt_latest_news_widget() {
    register_widget( 'Mwt_Latest_News_Widget' );
}
add_action( 'widgets_init', 'register_mwt_latest_news_widget' );