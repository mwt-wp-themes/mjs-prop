<?php

class Mwt_Top_Promo_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mwt_top_promo_widget', // Base ID
			esc_html__( 'MJS Top Promo', 'understrap' ), // Name
			array( 'description' => esc_html__( '', 'understrap' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
    global $mwt_option;
		//echo $args['before_widget'];  ?>

<section class="batasna_kabeh top_promo">
    <div class="batasna_wrap">
        <h3>Top Promo</h3>
        <div class="separator separator_abu"><span></span></div>
        <h2>Kami Telah Mengantar Tamu Allah Sejak Tahun 2008</h2>
        <h2 class="h2_kadua">Harga yang kami tawarkan adalah harga terbaik sesuai dengan standar Kementerian Agama Republik Indonesia</h2>

        <ul id="topPromo" style="display: block; opacity: 1;" class="owl-carousel owl-theme">

            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="width: 5940px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-330px, 0px, 0px);">
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/103/umroh-promo-20-november-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 25.200.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.800</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Promo 20 November 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Fajar Al Bade</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Langsung Madinah / Langsung Madinah">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/105/umroh-promo-06-november-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 25.200.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.800</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Promo 06 November 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsa</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="No transit / No transit">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/106/umroh-promo-22-desember-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 25.200.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.800</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Promo 22 Desember 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Fajr Al Bade</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="No Transit / No Transit">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/107/umroh-plus-turki" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 44.800.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>3.000</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Plus Turki</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsha</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Langsung Madinah / Langsung Madinah">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 11 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/101/umroh-reguler-26-desember-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 27.650.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.975</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Reguler 26 Desember 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsa</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Langsung Madinah / Langsung Madinah">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/108/umroh-plus-eropa-15-hari" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 65.250.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>4.500</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Plus Eropa 15 Hari</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsha</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Langsung Madinah / Langsung Madinah">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 15 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/100/umroh-reguler-24-desember-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 27.650.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.975</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Reguler 24 Desember 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsa</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="No Transit / No Transit">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/99/umroh-reguler-22-desember-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 27.650.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.975</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Reguler 22 Desember 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsa</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="No Transit / No Transit">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="owl-item" style="width: 330px;">
                        <li>
                            <a href="https://salamtour.com/umroh/102/umroh-promo-16-oktober-2018" title="">
                                <div class="tp_hulu">
                                    <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- Rp 25.200.000">
                                        <div class="tph_harga_blok">
                                            <span>USD</span>
                                            <p>1.800</p>
                                        </div>
                                    </div>

                                    <h4>Umroh Promo 16 Oktober 2018</h4>
                                </div>
                                <div class="tp_lis">
                                    <div class="clr"></div>

                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Mekah</span>
                                            <p>Al Marsa</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang 4">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Hotel Madinah</span>
                                            <p>Mubarok Silver</p>
                                        </div>

                                        <div class="tplnak_bentang bentang4"></div>
                                    </div>
                                    <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Langsung Madinah / Langsung Madinah">
                                        <div class="tplna_kenca tpl2ln"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <span>Pesawat</span>
                                            <p>Saudi Arabian Airlines</p>
                                        </div>
                                    </div>
                                    <div class="tplna">
                                        <div class="tplna_kenca"><i></i></div>
                                        <div class="tplna_katuhu">
                                            <p>Durasi 9 Hari</p>
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                </div>
                            </a>
                        </li>
                    </div>
                </div>
            </div>

        </ul>

        <div class="tp_kontrol">
            <a class="baten" id="prepTP"><i class="i_arahkenca"></i></a>
            <!--
		    		--><a href="https://salamtour.com/semua-jadwal" class="baten baten_dosis hint--rounded hint--biru hint--uppercase hint--bounce hint--bottom" data-hint="Lihat Semua Jadwal Umroh Lengkap"><span>Lihat Jadwal Umroh</span></a>
            <!--
		    		--><a class="baten" id="neksTP"><i class="i_arahkatuhu"></i></a>
        </div>

    </div>
</section>

		<?php
		//echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $term_id = ! empty( $instance['term_id'] ) ? $instance['term_id'] : '';
    $limit = ! empty( $instance['limit'] ) ? $instance['limit'] : 3;
    $sort = ! empty( $instance['sort'] ) ? $instance['sort'] : 'latest';
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'understrap' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'term_id' ) ); ?>"><?php esc_attr_e( 'Category:', 'nitnit' ); ?></label>	
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'term_id' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'term_id' ) ); ?>[]" multiple>
        <option value="0">-- All --</option>
				<?php $terms = get_terms( 'category', array(
              'hide_empty' => true,
          ) );
					foreach( $terms as $term ) {
						echo '<option value="'.$term->term_id.'" ';
            echo ( in_array( $term->term_id, $term_id ) ) ? 'selected' : '';
            echo '>'.$term->name.'</option>';
					}
				?>
			</select>
		</p>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php esc_attr_e( 'Limit:', 'understrap' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="number" value="<?php echo esc_attr( $limit ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'sort' ) ); ?>"><?php esc_attr_e( 'Sort:', 'nitnit' ); ?></label>	
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'sort' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'sort' ) ); ?>">
        <option value="latest">Latest</option>
        <option value="daily_view">Daily views</option>
        <option value="favorited">Favorited</option>
			</select>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['term_id'] = ( ! empty( $new_instance['term_id'] ) ) ?  $new_instance['term_id'] : [];
		return $instance;
	}

}

function register_mwt_top_promo_widget() {
    register_widget( 'Mwt_Top_Promo_Widget' );
}
add_action( 'widgets_init', 'register_mwt_top_promo_widget' );