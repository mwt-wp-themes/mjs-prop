<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Landing Page
 */

global $mwt, $mwt_option;
$page_id = get_the_ID();
$bg_image = ( has_post_thumbnail( $page_id ) ) ? get_the_post_thumbnail_url( $page_id ) : get_template_directory_uri() . '/assets/img/bg24.jpg';
get_header();
?>

	<div id="primary" class="content-area">
    
    <?php get_page_header('banner'); ?>
    
		<main id="main" class="site-main">

      <div class="section">
        <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-8 ml-auto mr-auto">
                <?php
                while ( have_posts() ) :
                  the_post();

                  get_template_part( 'template-parts/content', 'page' );

                  // If comments are open or we have at least one comment, load up the comment template.
            // 			if ( comments_open() || get_comments_number() ) :
            // 				comments_template();
            // 			endif;

                endwhile; // End of the loop.
                ?>

              </div>
            </div>
          </div>
        </div>

      </div>
    
    </main>
      
	</div><!-- #primary -->

<?php
get_footer();
