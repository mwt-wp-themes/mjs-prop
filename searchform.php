<?php
/**
 * The template for displaying search form
 */
 ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text"><i class="fa fa-search"></i></span>
    </div>
    <input type="search" name="s" class="form-control search-field" value="<?php echo get_search_query() ?>" placeholder="<?php echo esc_attr_x( 'Search...', 'jointswp' ) ?>" title="<?php echo esc_attr_x( 'Search for:', 'jointswp' ) ?>">
  </div>
<!--   <input type="submit" class="btn btn-primary btn-round" value="<?php echo esc_attr_x( 'Search', 'jointswp' ) ?>" /> -->
</form>