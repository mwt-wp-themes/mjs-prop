<?php global $mwt_option; ?>
<div class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto wow fadeIn">
        <?php echo $mwt_option['about-2-content']; ?>
      </div>
    </div>
  </div>
</div>