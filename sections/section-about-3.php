<?php global $mwt_option; ?>

<?php if( $mwt_option['about-3-enabled'] == 1 ): ?>
<div id="about-3" class="section" data-background-color="gray">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title wow fadeInDown"><?php echo $mwt_option['about-3-title']; ?></h2>
        <h4 class="description wow fadeInUp text-danger"><?php echo strip_tags( $mwt_option['about-3-subtitle'] ); ?></h5>
        <h5 class="description wow fadeInUp"><?php echo strip_tags( $mwt_option['about-3-description'] ); ?></h5>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto wow fadeIn">
        <br>
        <?php echo $mwt_option['about-3-content']; ?>
      </div>
    </div>
    <div class="row ml-auto mr-auto">
      <?php $images = ( !empty( $mwt_option['about-3-galery'] ) ) ? explode( ",", $mwt_option['about-3-galery'] ) : []; ?>
      <?php foreach( $images as $image_id ) : ?>
      <div class="col-md-6">
        <img class="rounded img-raised" src="<?php echo wp_get_attachment_url( $image_id ); ?>" style="margin:10px 0;">
      </div>
      <?php endforeach; ?>
    </div>
    <div class="section-space"></div>
    <div class="row">
      <div class="col-md-4">
        <h2 class="title text-center wow fadeInLeft"><?php echo $mwt_option['about-3-facility-title']; ?></h2>
      </div>
      <div class="col-md-8">
        <div class="row">
          <?php if( count( $mwt_option['about-3-facility'] ) > 0 ) : 
          foreach( $mwt_option['about-3-facility'] as $fasilitas ): ?>
          <div class="col-sm-12 col-md-6">
            <div class="info info-horizontal wow fadeInUp">
              <div class="icon icon-primary">
                <i class="now-ui-icons shopping_tag-content"></i>
              </div>
              <div class="description">
                <h4 class="info-title"><?php echo $fasilitas; ?></h4>
              </div>
            </div>
          </div>
          <?php endforeach; endif; ?>  
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>