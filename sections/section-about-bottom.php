<?php global $mwt_option; ?>
<?php if( $mwt_option['about-bottom-enabled'] == 1 ): ?>
<div id="about-bottom" class="section" data-background-color="gray">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto">
        <h2 class="title text-center wow fadeInDown"><?php echo $mwt_option['about-bottom-title']; ?></h2>
        <p class="text-justify wow fadeIn">
          <?php echo nl2br( $mwt_option['about-bottom-description'] ); ?>
        </p>
        <div class="benefit wow fadeInUp">
          <ul>
            <?php foreach( $mwt_option['about-bottom-benefit'] as $item ) : ?>
            <li>
              <span class="icon icon-primary"><i class="now-ui-icons ui-1_check"></i> </span>
              <?php echo $item; ?>
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>