<?php global $mwt_option; ?>

<?php if( $mwt_option['about-enabled'] == 1 ): ?>

<div id="about-top" class="section section-about-us">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title wow fadeInDown"><?php echo $mwt_option['about-title']; ?></h2>
        <h5 class="description wow fadeInUp"><?php echo strip_tags( $mwt_option['about-subtitle'] ); ?></h5>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <img class="rounded img-raised wow slideInLeft" src="<?php echo $mwt_option['about-image-1']['url']; ?>">
      </div>
      <div class="col-md-6">
        <img class="rounded img-raised wow slideInRight" src="<?php echo $mwt_option['about-image-2']['url']; ?>">
      </div>
    </div>
    <div class="section-space"></div>
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto text-center">
        <div class="wow fadeInDown">
          <?php echo $mwt_option['about-video-description']; ?>
        </div>
        <p><?php echo strip_tags( $mwt_option['about-description'] ); ?></p>
      </div>
    </div>
    <div class="separator separator-primary"></div>
  </div>
</div>

<div class="section section-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bg21.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto">
        <h2 class="title text-center wow fadeInDown"><?php echo $mwt_option['about-video-title']; ?></h2>
        <?php if( !empty( $mwt_option['about-youtube-video-id'] ) ) : ?>
        <div class="iframe-container">
          <iframe height="400" src="https://www.youtube.com/embed/<?php echo $mwt_option['about-youtube-video-id']; ?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<?php endif; ?>