<?php global $mwt_option; ?>

<?php if( $mwt_option['feature-bottom-enabled'] == 1 ): ?>
<div class="section section-image" id="feature-bottom" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bg22.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-md-6 ml-auto">
        <h2 class="title wow fadeInDown"><?php echo $mwt_option['feature-bottom-title']; ?></h2>
        <h4 class="description text-primary wow fadeInUp"><?php echo $mwt_option['feature-bottom-subtitle']; ?></h4>
        <p class="description wow fadeInRight"><?php echo $mwt_option['feature-bottom-description']; ?></p>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>