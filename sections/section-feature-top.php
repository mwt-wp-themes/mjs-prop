<?php global $mwt_option; ?>

<?php if( $mwt_option['feature-top-enabled'] == 1 ): ?>


<div class="features-6 section section-dark">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="info info-horizontal">
					<div class="icon icon-primary">
						<i class="now-ui-icons objects_support-17"></i>
					</div>
					<div class="description">
						<h4 class="info-title">Gratis Konsultasi</h4>
						<p>Konsultasi gratis dengan kami mengenai kebutuhan dan kriteria web atau aplikasi yang ingin anda miliki.</p>
					</div>
				</div>
				<div class="info info-horizontal">
					<div class="icon icon-primary">
						<i class="now-ui-icons objects_spaceship"></i>
					</div>
					<div class="description">
						<h4 class="info-title">Halaman Admin</h4>
						<p>Kelola situs anda dengan mudah melalui halaman administrator yang user friendly.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="phone-container">
          <img src="<?php echo $mwt_option['feature-top-image']['url']; ?>">
				</div>
			</div>
			<div class="col-md-4">
				<div class="info info-horizontal">
					<div class="icon icon-primary">
						<i class="now-ui-icons design_palette"></i>
					</div>
					<div class="description">
						<h4 class="info-title">Desain Profesional</h4>
						<p>Skill desain yang memadai membuat situs anda akan terlihat profesional</p>
					</div>
				</div>
				<div class="info info-horizontal">
					<div class="icon icon-primary">
						<i class="now-ui-icons gestures_tap-01"></i>
					</div>
					<div class="description">
						<h4 class="info-title">Responsive &amp; SEO Friendly</h4>
						<p>Tampilan situs menyesuaikan dengan perangkat yang digunakan pengunjung anda.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php endif; ?>