<?php global $mwt_option; ?>

<div id="form-pemesanan" class="section section-contact-us text-center">
  <div class="container">
    <h2 class="title wow fadeInUp"><?php echo $mwt_option['form-title']; ?></h2>
    <p class="description levitate"><?php echo $mwt_option['form-description']; ?></p>
    <div class="row">
      <div class="col-lg-7 text-center col-md-8 ml-auto mr-auto">
        <div id="form-peminat-msg"></div>
        <div class="row">
          <div class="col-sm-6">
            <div class="input-group input-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="now-ui-icons users_circle-08"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="Nama Lengkap" name="peminat_nama" id="peminat_nama" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="input-group input-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="now-ui-icons tech_mobile"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="No. Whatsapp Anda" name="peminat_hp" id="peminat_hp" required>
            </div>
          </div>
        </div>
        <div class="input-group input-lg">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="now-ui-icons ui-1_email-85"></i>
            </span>
          </div>
          <input type="text" class="form-control" placeholder="Email..." name="peminat_email" id="peminat_email" required>
        </div>
        <!--
        <div class="input-group input-lg d-none">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="now-ui-icons business_globe"></i>
            </span>
          </div>
          <input type="text" class="form-control" placeholder="Nama Perusahaan" name="peminat_company" id="peminat_company" required>
        </div>
        -->
        <div class="row">
          <div class="col text-left">
            <span class="text-muted" style="font-size:12px">Tentukan Jumlah Kavling</span>
            <?php if( intval( $mwt_option['form-kavling-count'] ) > 0 ) : ?>
            <div class="form-check form-check-radio">
              <?php for(  $i = 1; $i <= intval( $mwt_option['form-kavling-count'] ); $i++ ) :?>
              <label class="form-check-label">
                <input class="form-check-input" name="peminat_kavling" id="peminat_kavling" value="<?php echo $i; ?>" <?php echo ( $i == 1 ) ? 'checked' : '' ; ?> type="radio">
                <span class="form-check-sign"></span>
                <?php echo $i; ?> Kavling
              </label>
              <?php endfor; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
        <!--
        <div class="textarea-container d-none">
          <textarea class="form-control" name="peminat_domisili" id="peminat_domisili" rows="4" cols="80" placeholder="Alamat Domisili" required></textarea>
        </div>
        -->
        <div class="send-button">
          <button type="submit" id="submit-form-peminat" class="wow fadeIn btn btn-primary btn-round  btn-lg">Daftar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="verifikasiModal" tabindex="-1" role="dialog" aria-labelledby="verifikasiModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="verifikasiModalLabel">Verifikasi</h5>
      </div>
      <div class="modal-body">
        
        <p>Silahkan masukkan kode yang terkirim ke whatsapp anda.</p>
        
        <div id="form-verifikasi-msg"></div>
        
        <div class="row" id="form-verifikasi-wa">
          <div class="col-sm-8">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="now-ui-icons ui-1_email-85"></i></span>
              </div>
              <input class="form-control" id="kode_verifikasi" name="kode_verifikasi" placeholder="6 digit kode" type="text">
            </div>
          </div>
          <div class="col-sm-4">
            <input type="hidden" name="submit_id" id="submit_id" value="">
            <button id="submit-verifikasi-hp" type="button" class="btn btn-primary btn-round btn-block">Verifikasi</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>