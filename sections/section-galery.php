<?php global $mwt_option; ?>

<?php if( $mwt_option['galery-enabled'] == 1 ): ?>
<div class="section" id="galery">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title wow fadeInDown"><?php echo $mwt_option['galery-title']; ?></h2>
        <h5 class="description wow fadeInUp"><?php echo strip_tags( $mwt_option['galery-description'] ); ?></h5>
        <?php $images = ( !empty( $mwt_option['galery-items'] ) ) ? explode( ",", $mwt_option['galery-items'] ) : []; ?>
        <?php if( count( $images ) > 0 ) : ?>
        <div id="gallery" class="owl-carousel owl-theme">
              <?php foreach( $images as $image_id ) : ?>
              <div class="item gallery">
                <img class="wow fadeIn" src="<?php echo wp_get_attachment_url( $image_id ); ?>">
              </div>
              <?php $count++; endforeach; ?>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
