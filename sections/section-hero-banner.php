<?php global $mwt_option; ?>

<div class="page-header header-filter wow fadeIn">
  <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo $mwt_option['hero-banner-img']['url']; ?>');">
  </div>
  <div class="content-center">
    <div class="container">
      <?php echo ( !empty( $mwt_option['hero-banner-title'] ) ) ? '<h1 class="title">' . $mwt_option['hero-banner-title'] . '</h1>' : ''; ?>
      <?php echo ( !empty( $mwt_option['hero-banner-description'] ) ) ? '<h4 class="description">' . $mwt_option['hero-banner-description'] . '</h4>' : ''; ?>
    </div>
  </div>
</div>