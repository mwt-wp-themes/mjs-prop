<?php global $mwt_option; ?>

<?php if( $mwt_option['hero-enabled'] == 1 ): ?>
<div id="carouselExampleIndicators" class="carousel slide">
  <?php /*
  <ol class="carousel-indicators">
    <?php foreach( $mwt_option['slide-items'] as $key => $slide ) : ?>
    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $key; ?>" class="<?php echo ( $key == 0 ) ? 'active' : ''; ?>"></li>
    <?php endforeach; ?>
  </ol>
  */ ?>
  <div class="carousel-inner" role="listbox">
    <?php
    // WP_Query arguments
    $args = array(
      'post_type'             => array( 'info-content' ),
      'post_status'           => array( 'publish' ),
      'tax_query' => array(  
      'relation' => 'AND',   
        array(
          'taxonomy' => 'info_content_category',
          'field' => 'id',
          'terms' => array( $mwt_option['hero-info-content-category'] ),
          'include_children' => false,
          'operator' => 'IN'
        )
      ),
      'order'                 => 'ASC'
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) : $count = 0; 
     while ( $query->have_posts() ) : $query->the_post(); ?>
    <div class="carousel-item <?php echo ( $count == 0 ) ? 'active' : ''; ?>">
      <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
        <div class="content-center">
          <div class="container">
            <div class="content-center">
              <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                  <h1 class="title"><?php the_title(); ?></h1>
                  <h4 class="description "><?php echo strip_tags( get_the_content() ); ?></h4>
<!--                   <br />
                  <div class="buttons">
                    <a href="#pablo" class="btn btn-icon btn-neutral btn-danger btn-round mt-2">
                    <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#pablo" class="btn btn-icon btn-neutral btn-danger btn-round mt-2">
                    <i class="fab fa-facebook-square"></i>
                    </a>
                    <a href="#pablo" class="btn btn-icon btn-neutral btn-danger btn-round mt-2">
                    <i class="fab fa-google-plus"></i>
                    </a>
                    <a href="#pablo" class="btn btn-icon btn-neutral btn-danger btn-round  mt-2">
                    <i class="fab fa-instagram"></i>
                    </a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php $count++; 
    endwhile; endif;
    // Restore original Post Data
    wp_reset_postdata(); ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
  <i class="now-ui-icons arrows-1_minimal-left"></i>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
  <i class="now-ui-icons arrows-1_minimal-right"></i>
  </a>
</div>
<?php endif; ?>