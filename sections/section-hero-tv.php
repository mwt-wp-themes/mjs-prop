<?php global $mwt_option; ?>

<div class="header-3">
  <div class="page-header header-filter wow fadeIn">
    <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo $mwt_option['hero-tv-img']['url']; ?>');">
    </div>
    <div class="content-center">
      <div class="container text-left">
        <div class="content-center">
          <div class="row">
            <div class="col-md-6 ml-auto mr-auto text-left">
              <h1 class="title"><?php echo $mwt_option['hero-tv-title']; ?></h1>
              <h4 class="description "><?php echo $mwt_option['hero-tv-description']; ?></h4>
            </div>
            <div class="col-md-5 text-right">
              <?php if( !empty( $mwt_option['hero-tv-video'] ) ) : ?>
              <div class="iframe-container">
                <iframe src="https://www.youtube.com/embed/<?php echo $mwt_option['hero-tv-video']; ?>?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" height="250" frameborder="0"></iframe>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>