<?php global $mwt_option; ?>

<?php if( $mwt_option['hero-type'] == 'slider' ): ?>
  <?php get_template_part( 'sections/section', 'hero-sliders' ); ?>
<?php elseif( $mwt_option['hero-type'] == 'banner' ): ?>
  <?php get_template_part( 'sections/section', 'hero-banner' ); ?>
<?php elseif( $mwt_option['hero-type'] == 'tv' ): ?>
  <?php get_template_part( 'sections/section', 'hero-tv' ); ?>
<?php endif; ?>