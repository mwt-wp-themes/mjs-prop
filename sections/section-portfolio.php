<?php global $mwt_option; ?>

<?php if( $mwt_option['portfolio-enabled'] == 1 ): ?>

<?php
// WP_Query arguments
$args = array(
  'post_type'         => array( 'portfolio' ),
  'post_status'       => array( 'publish' ),
  'nopaging'          => true,
  'posts_per_page'    => 9
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) : $count = 0; ?>

<div class="projects-2 section-image" style="background-image: url('<?php echo $mwt_option['portfolio-bg-image']['url']; ?>')">
	<div class="container">
		<div class="row">
			<div class="col-md-8 ml-auto mr-auto text-center">
				<h2 class="title"><?php echo $mwt_option['portfolio-title']; ?></h2>
				<div class="section-space"></div>
			</div>
		</div>
		<div class="row">
			<?php while ( $query->have_posts() ) : $query->the_post(); 
      $url = Mwt::get_field( 'url', get_the_ID() );
      $url = ( !empty( $url ) ) ? $url : '#';
      ?>
			<div class="col-md-4">
				<div class="card card-plain">
					<a href="<?php echo $url; ?>" target="_blank" rel="nofollow">
						<div class="card-image">
							<img class="img-raised rounded" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
						</div>
					</a>
					<div class="card-body">
						<a href="<?php echo $url; ?>" target="_blank" rel="nofollow">
							<?php the_title('<h4 class="card-title">', '</h4>'); ?>
						</a>
						<h6 class="category text-primary">Web Desain</h6>
						<p class="card-description">
						</p>
					</div>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php endif;
// Restore original Post Data
wp_reset_postdata(); ?>

<?php endif; ?>
