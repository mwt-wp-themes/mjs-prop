<?php global $mwt_option; ?>

<?php if( $mwt_option['reason-enabled'] == 1 ): ?>
<div class="section" id="reason">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h2 class="title wow fadeInDown"><?php echo $mwt_option['reason-title']; ?></h2>
        <h4 class="description wow fadeInUp"><?php echo $mwt_option['reason-description']; ?></h4>
      </div>
      <div class="col-md-8">
        <div class="row">
          <?php if( count( $mwt_option['reason-content'] ) > 0 ) : 
          foreach( $mwt_option['reason-content'] as $reason ): ?>
          <div class="col-sm-12 col-md-6">
            <div class="info info-horizontal wow fadeInUp">
              <div class="icon icon-primary">
                <i class="now-ui-icons shopping_tag-content"></i>
              </div>
              <div class="description">
                <h4 class="info-title"><?php echo $reason; ?></h4>
              </div>
            </div>
          </div>
          <?php endforeach; endif; ?>  
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>