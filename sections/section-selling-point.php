<?php global $mwt_option; ?>

<?php if( $mwt_option['selling-point-enabled'] == 1 ): ?>

<div class="section" id="selling-point" data-background-color="primary">
  <div class="container">
    <div class="row">
      <?php
      // WP_Query arguments
      $args = array(
        'post_type'             => array( 'info-content' ),
        'post_status'           => array( 'publish' ),
        'tax_query' => array(  
        'relation' => 'AND',   
          array(
            'taxonomy' => 'info_content_category',
            'field' => 'id',
            'terms' => array( $mwt_option['selling-point-info-content-category'] ),
            'include_children' => false,
            'operator' => 'IN'
          )
        ),
        'order'                 => 'ASC'
      );

      // The Query
      $query = new WP_Query( $args );

      // The Loop
      if ( $query->have_posts() ) : $count = 0; 
       while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="col-sm-12 col-md-6 col-lg-3">


        <div class="card card-blog  wow fadeIn" data-wow-duration="3s">
          <div class="card-image">
            <a href="<?php echo get_the_post_thumbnail_url(); ?>">
              <?php the_post_thumbnail( array( 416, 234 ), array( 'class' => 'img rounded' ) ); ?>
            </a>
          </div>
          <div class="card-body text-center">
    <!--         <h6 class="category text-danger">
              <i class="now-ui-icons media-2_sound-wave"></i> Business
            </h6> -->
            <h5 class="card-title"><?php the_title(); ?></h5>
<!--             <p class="card-description"></p> -->
          </div>
        </div>


      </div>
      <?php $count++; 
      endwhile; endif;
      // Restore original Post Data
      wp_reset_postdata(); ?>
    </div>
  </div>
</div>

<?php endif; ?>