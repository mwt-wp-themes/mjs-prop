<?php
global $mwt_option;

if( $mwt_option['testimonial-enabled'] == 1 ) :

// WP_Query arguments
$args = array(
  'post_type'              => array( 'testimonial' ),
  'post_status'            => array( 'publish' ),
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) : $count = 0; ?>

<div class="testimonials-2">

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div id="carouselExampleIndicators2" class="carousel slide">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators2" data-slide-to="1" class=""></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="carousel-item justify-content-center <?php echo ( $count == 0 ) ? 'active' : '' ; ?>">
                            <div class="card card-testimonial card-plain">
                                <div class="card-avatar">
                                    <a href="javascript:void(0)">
                                      <img src="<?php echo get_avatar_url( get_field('email') ); ?>" class="img img-raised rounded">
                                    </a>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-description"><?php the_content(); ?></h5>
                                    <?php the_title('<h3 class="card-title">', '</h3>'); ?>
                                    <!--
                                    <div class="card-footer">
                                        <h6 class="category text-primary"></h6>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                        <?php $count++; endwhile; ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                        <i class="now-ui-icons arrows-1_minimal-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                        <i class="now-ui-icons arrows-1_minimal-right"></i>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<?php endif;
// Restore original Post Data
wp_reset_postdata(); 
endif;
?>