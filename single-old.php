<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MJS Landing Page
 */

global $mwt, $mwt_option;
$post_id = get_the_ID();
$bg_image = ( has_post_thumbnail( $post_id ) ) ? get_the_post_thumbnail_url( $post_id ) : get_template_directory_uri() . '/assets/img/bg1.jpg';
get_header();
?>

<div class="main main-raised">
  <div class="container">

    <main>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <?php
            while ( have_posts() ) :
              the_post();

              get_template_part( 'template-parts/content', 'single' );

              the_post_navigation();

              // If comments are open or we have at least one comment, load up the comment template.
              if ( comments_open() || get_comments_number() ) :
                comments_template();
              endif;

            endwhile; // End of the loop.
            ?>

          </div>
        </div>
      </div>
    </div>
  <!--     
    <div class="section section-blog-info">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <div class="row">
              <div class="col-md-6">
                <div class="blog-tags">
                  Tags:
                  <span class="label label-primary">Photography</span>
                  <span class="label label-primary">Stories</span>
                  <span class="label label-primary">Castle</span>
                </div>
              </div>
              <div class="col-md-6">
                <a href="#pablo" class="btn btn-google btn-round pull-right">
                <i class="fab fa-google"></i> 232
                </a>
                <a href="#pablo" class="btn btn-twitter btn-round pull-right">
                <i class="fab fa-twitter"></i> 910
                </a>
                <a href="#pablo" class="btn btn-facebook btn-round pull-right">
                <i class="fab fa-facebook-square"></i> 872
                </a>
              </div>
            </div>
            <hr />
            <div class="card card-profile card-plain">
              <div class="row">
                <div class="col-md-2">
                  <div class="card-avatar">
                    <a href="#pablo">
                    <img class="img img-raised" src="../assets/img/mike.jpg">
                    </a>
                    <div class="ripple-container"></div>
                  </div>
                </div>
                <div class="col-md-8">
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="description">I've been trying to figure out the bed design for the master bedroom at our Hidden Hills compound...I like good music from Youtube.</p>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-default pull-right btn-round">Follow</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section section-comments">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <div class="media-area">
              <h3 class="title text-center">3 Comments</h3>
              <div class="media">
                <a class="pull-left" href="#pablo">
                  <div class="avatar">
                    <img class="media-object img-raised" src="../assets/img/james.jpg" alt="..."/>
                  </div>
                </a>
                <div class="media-body">
                  <h5 class="media-heading">Tina Andrew <small class="text-muted">&middot; 7 minutes ago</small></h5>
                  <p>Chance too good. God level bars. I'm so proud of @LifeOfDesiigner #1 song in the country. Panda! Don't be scared of the truth because we need to restart the human foundation in truth I stand with the most humility. We are so blessed!</p>
                  <p>All praises and blessings to the families of people who never gave up on dreams. Don't forget, You're Awesome!</p>
                  <div class="media-footer">
                    <a href="#pablo" class="btn btn-primary btn-neutral pull-right" rel="tooltip" title="Reply to Comment">
                    <i class="now-ui-icons ui-1_send"></i> Reply
                    </a>
                    <a href="#pablo" class="btn btn-danger btn-neutral pull-right">
                    <i class="now-ui-icons ui-2_favourite-28"></i> 243
                    </a>
                  </div>
                </div>
              </div>
              <div class="media">
                <a class="pull-left" href="#pablo">
                  <div class="avatar">
                    <img class="media-object img-raised" alt="Tim Picture" src="../assets/img/michael.jpg">
                  </div>
                </a>
                <div class="media-body">
                  <h5 class="media-heading">John Camber <small class="text-muted">&middot; Yesterday</small></h5>
                  <p>Hello guys, nice to have you on the platform! There will be a lot of great stuff coming soon. We will keep you posted for the latest news.</p>
                  <p> Don't forget, You're Awesome!</p>
                  <div class="media-footer">
                    <a href="#pablo" class="btn btn-primary btn-neutral pull-right" rel="tooltip" title="Reply to Comment">
                    <i class="now-ui-icons ui-1_send"></i> Reply
                    </a>
                    <a href="#pablo" class="btn btn-danger btn-neutral pull-right">
                    <i class="now-ui-icons ui-2_favourite-28"></i> 25
                    </a>
                  </div>
                  <div class="media">
                    <a class="pull-left" href="#pablo">
                      <div class="avatar">
                        <img class="media-object img-raised" alt="64x64" src="../assets/img/julie.jpg">
                      </div>
                    </a>
                    <div class="media-body">
                      <h5 class="media-heading">Tina Andrew <small class="text-muted">· 2 Days Ago</small></h5>
                      <p>Hello guys, nice to have you on the platform! There will be a lot of great stuff coming soon. We will keep you posted for the latest news.</p>
                      <p> Don't forget, You're Awesome!</p>
                      <div class="media-footer">
                        <a href="#pablo" class="btn btn-primary btn-neutral pull-right" rel="tooltip" title="" data-original-title="Reply to Comment">
                        <i class="now-ui-icons ui-1_send"></i> Reply
                        </a>
                        <a href="#pablo" class="btn btn-danger btn-neutral pull-right">
                        <i class="now-ui-icons ui-2_favourite-28"></i> 2
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h3 class="title text-center">Post your comment</h3>
            <div class="media media-post">
              <a class="pull-left author" href="#pablo">
                <div class="avatar">
                  <img class="media-object img-raised" alt="64x64" src="../assets/img/olivia.jpg">
                </div>
              </a>
              <div class="media-body">
                <textarea class="form-control" placeholder="Write a nice reply or go home..." rows="4"></textarea>
                <div class="media-footer">
                  <a href="#pablo" class="btn btn-primary pull-right">
                  <i class="now-ui-icons ui-1_send"></i> Reply
                  </a>
                </div>
              </div>
            </div>
            <!-- end media-post -->
          </div>
        </div>
      </div>
    </div> -->
  </div>

    </main>

  </div>
</div>

<?php
get_footer();
