<?php
/**
 * Template name: Account
 */

global $mwt, $mwt_option;
if( !is_user_logged_in() ) {
  wp_redirect( get_permalink( $mwt_option['login-page-id'] ) );
}

$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : 'profile';
$user_info = get_userdata( get_current_user_id() );

// Set the image size. Accepts all registered images sizes and array(int, int)
$size = 'thumbnail';
// Get the image URL using the author ID and image size params
$avatar_url = get_cupp_meta( $user_info->ID, $size );
if( empty( $avatar_url ) )
  $avatar_url = get_avatar_url( $user_info->ID );

$user_role = implode(', ', $user_info->roles);
$role = strtolower( str_replace( ' ', '_', $user_role ) );
if( $role == 'staf_cs' )  {
 $role = 'staf_mjs'; 
} elseif( $role == "mitra" ) {
  $path = parse_url(get_option('siteurl'), PHP_URL_PATH);
  $host = parse_url(get_option('siteurl'), PHP_URL_HOST);
  $expiry = strtotime('+1 month');
  setcookie( '_mjs_mitra', $user_info->ID, $expiry, $path, $host );
}

get_header();
?>

			<div class="page-header page-header-small header-filter" filter-color="orange">
				<div class="page-header-image" data-parallax="true" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/bg5.jpg');">
				</div>
				<div class="container">
					<div class="photo-container">
						<img src="<?php echo $avatar_url; ?>" alt="<?php echo $user_info->display_name; ?>">
					</div>
					<h3 class="title"><?php echo $user_info->display_name; ?></h3>
					<p class="category"><?php echo $user_role; ?></p>
				</div>
			</div>
			<div class="section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
              <div class="row">
                  <div class="col-sm-12 col-md-3">
                    <br><br>
                    <div class="pure-menu custom-restricted-width">
                        <ul class="list-group">
                          <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'profile', get_permalink() ); ?>">My Profile</a>
                          <?php if( $role == "mitra" ) : ?>
                            <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'sales', get_permalink() ); ?>">My Customer</a>
                          <?php elseif( $role == "staf_mjs" ) :?>
                          <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'assignment', get_permalink() ); ?>">Assignments</a>
                          <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'mitra', get_permalink() ); ?>">Data Mitra</a>
                          <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'add-mitra', get_permalink() ); ?>">Tambah Mitra</a>
                          <?php elseif( $role == "staf_spv" || $role == "staf_manager" ) :?>
                          <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'assignment', get_permalink() ); ?>">Assignments</a>
                          <?php endif; ?>
                            <a class="list-group-item" href="<?php echo add_query_arg( 'section', 'ganti-password', get_permalink() ); ?>">Ganti Password</a>
                            <a class="list-group-item" href="<?php echo wp_logout_url("/"); ?>" onclick="return confirm('Yakin ingin logout?');">Logout</a>
                        </ul>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-9">
                    <h4 style="margin-top: 0;text-align:left" class="title">
                      <?php if( !empty( $section ) ) : ?>
                      <?php echo ucwords( str_replace(["_", "-"], " ", $section) ); ?>
                      <?php else : ?>
                      Dashboard                      
                      <?php endif; ?>
                    </h4>
                    <?php 
                    if( in_array( $role, ['mitra', 'staf_mjs', 'staf_spv', 'staf_manager'] ) ) {
                      get_template_part( 'template-parts/content', 'account-' . $role ); 
                    } ?>

                    <?php if( $section == "profile" ) : ?>


                    <?php user_update_profile_action(); ?>

                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input id="username" type="text" value="<?php echo $user_info->user_login; ?>" class="form-control" disabled>
                            </div>

                            <div class="form-group">
                                <label for="first_name">Nama Lengkap</label>
                                <input id="first_name" type="text" placeholder="Full name" name="first_name" value="<?php echo $user_info->first_name . ' ' . $user_info->last_name; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input id="email" type="email" value="<?php echo $user_info->user_email; ?>" disabled class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="nomor_telp">Nomor Telp</label>
                                <input id="nomor_telp" name="nomor_telp" type="text" value="<?php echo get_user_meta( $user_info->ID, '_nomortelp', true ); ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="nomor_hp">Nomor HP</label>
                                <input id="nomor_hp" name="nomor_hp" type="text" value="<?php echo get_user_meta( $user_info->ID, '_nomorhp', true ); ?>" class="form-control">
                            </div>

                            <?php if( $role == "mitra" ) : ?>
                            <div class="form-group">
                              <p>Rekening Bank (Untuk penerimaan komisi)</p>
                            </div>
                            <div class="form-group">
                                <label for="nama_bank">Nama Bank</label>
                                <input id="nama_bank" name="nama_bank" type="text" value="<?php echo get_user_meta( $user_info->ID, '_nama_bank', true ); ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nomor_rekening">Nomor Rekening</label>
                                <input id="nomor_rekening" name="nomor_rekening" type="text" value="<?php echo get_user_meta( $user_info->ID, '_norek', true ); ?>" class="form-control">
                            </div>
                            <?php endif; ?>

                            <div class="form-group">

                                <button name="submit" type="submit" class="btn btn-fullcolor btn--rounded">Submit</button>
                            </div>
                    </form>

                    <?php elseif( $section == "ganti-password" ) : 
                    if( isset( $_POST['submit-password'] ) ) {
                      $user_id = get_current_user_id();
                      $password = $_POST['password'];
                      wp_set_password( $password, $user_id );
                      echo '<div class="alert alert-success">';
                      echo 'Password anda berhasil diganti.';
                      echo '</div>';
                    }
                    ?>

                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                        <div class="form-group">
                            <label for="password">Password baru</label>
                            <input id="password" type="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="confirm_password">Ulangi password</label>
                            <input id="confirm_password" type="password" name="password" class="form-control">
                            <span id="message" class="pure-form-message-inline"></span>
                        </div>

                        <div class="form-group">
                            <button id="submit-password" name="submit-password" type="submit" class="btn btn-fullcolor btn--rounded" disabled="true">Submit</button>
                        </div>
                    </form>

                    <?php endif; ?>

                  </div>
              </div>
              
						</div>
					</div>
				</div>
			</div>

<?php
get_footer();
