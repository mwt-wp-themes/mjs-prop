<?php
/**
 * Template name: Contact us
 */

global $mwt_option;
get_header();
?>

<div class="main">


  <div class="contact-content">
    <div class="container">
      <div class="row">
        <div class="col-md-5 ml-auto mr-auto">
          <h2 class="title">Hubungi kami</h2>
          <p class="description">Silakan isi form dibawah dan untuk menyampaikan pesan atau keinginan Anda kepada kami. Privasi Anda adalah salah satu prioritas kami dan data Anda tidak akan pernah disalahgunakan atau dibagikan kepada pihak ketiga.<br><br>
          </p>
          <form name="form" method="post" id="contact-form" role="form">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="now-ui-icons users_circle-08"></i></span>
              </div>
              <input type="text" id="form_name" name="form[name]" required="required" placeholder="Name" class="form-control" aria-label="Name" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="now-ui-icons ui-1_email-85"></i></span>
              </div>
              <input type="email" id="form_email" name="form[email]" required="required" placeholder="Email" class="form-control" aria-label="Email">
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="now-ui-icons tech_mobile"></i></span>
              </div>
              <input type="text" id="form_phone" name="form[phone]" required="required" placeholder="Nomor Telp/HP" class="form-control">
            </div>
            <div class="form-group">
              <textarea id="form_message" name="form[message]" required="required" placeholder="Tell us your thoughts and feelings..." class="form-control" rows="6"></textarea>
            </div>
            <div class="submit text-center">
              <input class="btn btn-primary btn-raised btn-round" value="Kirim" type="submit">
            </div>
            <input type="hidden" id="form__token" name="form[_token]" value="yunpTuFJriAsu8ncBH7uN5J_vN7UATdz95H7lSETf8o">
          </form>
        </div>
        <div class="col-md-5 ml-auto mr-auto">
          <div class="info info-horizontal mt-5">
            <div class="icon icon-primary">
              <i class="now-ui-icons location_pin"></i>
            </div>
            <div class="description">
              <h4 class="info-title">CV Media Web Technology</h4>
              <p> <strong></strong>
                Jalan Merpati No. 144<br>
                Parepare, SN 91125<br>
                HP: <strong>0822 9220 6764</strong> <em>(Call/Sms/WA)</em>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
<?php
get_footer();
