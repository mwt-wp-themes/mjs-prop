<?php
/**
 * Template name: Home
 */

global $mwt, $mwt_option;
get_header();
?>

  <?php get_template_part( 'sections/section', 'hero' ); ?>

  <?php get_template_part( 'sections/section', 'feature-top' ); ?>

  <?php //get_template_part( 'sections/section', 'selling-point' ); ?>

  <?php //get_template_part( 'sections/section', 'about-top' ); ?>

  <?php //get_template_part( 'sections/section', 'galery' ); ?>

  <?php //get_template_part( 'sections/section', 'about-bottom' ); ?>

  <?php get_template_part( 'sections/section', 'portfolio' ); ?>

  <?php //get_template_part( 'sections/section', 'reason' ); ?>

  <?php //get_template_part( 'sections/section', 'feature-bottom' ); ?>

  <?php //get_template_part( 'sections/section', 'about-3' ); ?>

  <?php get_template_part( 'sections/section', 'testimonials' ); ?>

  <?php //get_template_part( 'sections/section', 'contact' ); ?>

<?php
get_footer();
