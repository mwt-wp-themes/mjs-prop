<?php
/**
 * Template name: Login
 */
global $mwt_option;
get_header();
?>

<div class="page-header header-filter" filter-color="orange">
  <div class="page-header-image" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/login.jpg)"></div>
  <div class="content">
    <div class="container">
      <div class="col-md-5 ml-auto mr-auto">
        <div class="card card-login card-plain">
          <?php echo do_shortcode("[mwt-login-form]"); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
get_footer();
