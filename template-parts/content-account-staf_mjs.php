<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option, $user_info;
$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : 'profile';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if( $section == "assignment" ) : ?>

    <table id="datatable1" class="table">
        <thead>
            <tr>
                <th width="25">#</th>
                <th>Tanggal</th>
                <th>Customer</th>
                <th>Kavling</th>
                <th>Contact</th>
                <th>Mitra</th>
                <th>Follow Up</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>
          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'participants' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
            'meta_key'              => 'staf',
            'meta_value'            => $user_info->ID,
            'meta_value_num'            => $user_info->ID,
            'meta_compare'          => '='
          );

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 1;
            while ( $query->have_posts() ) {
              $query->the_post(); 
              $paket = Mwt::get_field( 'paket_umroh' );  ?>
              <tr>
                  <td><?php echo $count; ?></td>
                  <td><?php echo get_the_date('d/m/Y'); ?></td>
                  <td><?php the_title(); ?></td>
                  <td><?php echo Mwt::get_field('kavling'); ?></td>
                  <td>
                    Email: <strong><?php echo Mwt::get_field('email'); ?></strong><br>
                    HP: <strong><?php echo Mwt::get_field('nomor_hp'); ?></strong>
                  </td>
                  <td><?php echo ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->display_name : ''; ?></td>
                  <td>
                    <a href="#" class="followup-result-input" data-type="textarea" data-pk="<?php the_ID(); ?>"><?php echo Mwt::get_field( 'follow_up_result' ); ?></a>
                  </td>
                  <td>
                    <a href="#" class="cust-status" data-type="select" data-pk="<?php the_ID(); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>?action=mwt_update_cust_status" data-title="Select status"><?php echo ( Mwt::get_field( 'status' ) == "Confirmed" ) ? 'Confirmed' : 'Pending'; ?></a>
                    <br><span class="spv-field-<?php the_ID(); ?>" style="<?php echo ( Mwt::get_field( 'status' ) == "Confirmed" ) ? '' : 'display:none'; ?>">
                    SPV: 
                    <?php 
                    $spv = Mwt::get_field('spv');
                    echo '<a href="#" class="supervisor-field" data-type="select" data-pk="'.get_the_ID().'" data-url="'.admin_url('admin-ajax.php').'?action=mwt_assign_spv" data-title="Pilih Supervisor">';
                    echo ( !empty( $spv ) ) ? $spv->display_name : '';   
                    echo '</a>';
                    ?></span>
                  </td>
                  <!--
                  <td>
                    <?php if( Mwt::get_field( 'status' ) == "Confirmed" ) : ?>
                    <?php 
                    $spv = Mwt::get_field('spv');
                    echo '<a href="#" class="supervisor-field" data-type="select" data-pk="'.get_the_ID().'" data-url="'.admin_url('admin-ajax.php').'?action=mwt_assign_spv" data-title="Pilih Supervisor">';
                    echo ( !empty( $spv ) ) ? $spv->display_name : '';   
                    echo '</a>';
                    ?>
                    <?php else: ?>
                    -
                    <?php endif; ?>
                  </td>
                  -->
              </tr>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>
        </tbody>
    </table>
  
    <?php elseif( $section == "mitra" ) : ?>
  
      <?php if( !empty( $_GET['edit'] ) ) : ?>
  
        <?php
        $mitra = get_user_by( 'ID', $_GET['edit'] );
        if( !$mitra ) {
          echo '<div class="alert alert-error">';
          echo 'Mitra tidak ditemukan!';
          echo '</div>';
        } else {
          if( isset( $_POST['update-mitra'] ) ) {
            $user_id = intval( $_GET['edit'] );
            $full_name  =   sanitize_text_field( $_POST['full_name'] );
            $alamat     =   esc_textarea( $_POST['alamat'] );
            $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
            $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );

            $user_id = wp_update_user( array( 
              'ID'          => $user_id, 
              'first_name'  => $full_name,
              'last_name'   => '',
            ) );

            if ( is_wp_error( $user_id ) ) {
              // There was an error, probably that user doesn't exist.
            } else {
              // Success!
              update_user_meta( $user_id, '_alamat', $alamat );
              update_user_meta( $user_id, '_nomorhp', $nomor_hp );
              update_user_meta( $user_id, '_nomortelp', $nomor_wa );
              update_user_meta( $user_id, '_staf', get_current_user_id() );
              if( !empty( $_POST['nama_bank'] ) ) {
                update_user_meta( $user_id, '_nama_bank', $_POST['nama_bank'] );
              }
              if( !empty( $_POST['nomor_rekening'] ) ) {
                update_user_meta( $user_id, '_norek', $_POST['nomor_rekening'] );
              }
              echo '<div class="alert alert-error">';
              echo 'Data mitra berhasil diupdate.';
              echo '</div>';
            }	
          }
        ?>
  
        <form method="post" action="#" class="form-horizontal">
                <div class="form-group row">
                    <label for="full_name" class="col-sm-2 col-form-label">Nama Lengkap</label>
                    <div class="col-sm-10">
                      <input id="full_name" name="full_name" type="text" value="<?php echo $mitra->first_name . ' ' . $user->last_name; ?>" required class="form-control">  
                    </div>
                </div>

                <div class="form-group row">
                    <label for="username" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                      <input id="username" type="text" name="username" value="<?php echo ( isset( $mitra->user_login ) ? $mitra->user_login : null ); ?>" disabled class="form-control">  
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input id="email" type="email" name="email" value="<?php echo $mitra->user_email; ?>" disabled class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea id="alamat" name="alamat" class="form-control"><?php echo get_user_meta( $mitra->ID, '_alamat', true ); ?></textarea>  
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nomor_hp" class="col-sm-2 col-form-label">Nomor HP</label>
                    <div class="col-sm-10">
                      <input id="nomor_hp" name="nomor_hp" type="text" value="<?php echo get_user_meta( $mitra->ID, '_nomorhp', true ); ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nomor_wa" class="col-sm-2 col-form-label">Nomor WA</label>
                    <div class="col-sm-10">
                      <input id="nomor_wa" name="nomor_wa" type="text" value="<?php echo get_user_meta( $mitra->ID, '_nomortelp', true ); ?>" class="form-control">
                    </div>
                </div>
          
                <div class="offset-sm-2 col-sm-10">
                <p>
                  Rekening Bank (Untuk penerimaan komisi)
                  </p>
                </div>
                <div class="form-group row">
                    <label for="nama_bank" class="col-sm-2 col-form-label">Nama Bank</label>
                    <div class="col-sm-10">
                      <input id="nama_bank" name="nama_bank" type="text" value="<?php echo get_user_meta( $mitra->ID, '_nama_bank', true ); ?>" class="form-control">  
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nomor_rekening" class="col-sm-2 col-form-label">Nomor Rekening</label>
                    <div class="col-sm-10">
                      <input id="nomor_rekening" name="nomor_rekening" type="text" value="<?php echo get_user_meta( $mitra->ID, '_norek', true ); ?>" class="form-control">  
                    </div>
                </div>

                <div class="offset-sm-2 col-sm-10">
                    <button type="submit" name="update-mitra" class="btn btn-fullcolor btn--rounded">Submit</button>
                </div>
        </form>
  
        <?php } ?>
  
      <?php else: ?>
      
      <table id="datatable1" class="table">
          <thead>
              <tr>
                  <th width="25">#</th>
                  <th>Tanggal Daftar</th>
                  <th>Username</th>
                  <th>Nama Lengkap</th>
                  <th>Email</th>
                  <th>Nomor HP</th>
                  <th>Nomor WA</th>
                  <th>Rekening Bank</th>
                  <th></th>
              </tr>
          </thead>

          <tbody>
            <?php
            $args = array(
              'role'           => 'Mitra',
              'meta_key'      => '_staf',
              'meta_value'    => get_current_user_id(),
              'meta_value_num'=> get_current_user_id(),
              'meta_compare'  => '='
            );

            // The User Query
            $user_query = new WP_User_Query( $args );

            // The User Loop
            if ( ! empty( $user_query->results ) ) {
              $count = 1;
              foreach ( $user_query->results as $user ) { ?>

                <tr>
                    <td><?php echo $count; ?></td>
                    <td><?php echo get_the_date('d/m/Y'); ?></td>
                    <td><?php echo $user->user_login; ?></td>
                    <td><?php echo $user->first_name . ' ' . $user->last_name; ?></td>
                    <td><?php echo $user->user_email; ?></td>
                    <td><?php echo get_user_meta( $user->ID, '_nomortelp', true ); ?></td>
                    <td><?php echo get_user_meta( $user->ID, '_nomorhp', true ); ?></td>
                    <td>
                      <?php echo get_user_meta( $user->ID, '_nama_bank', true ); ?> - <?php echo get_user_meta( $user->ID, '_norek', true ); ?>
                    </td>
                    <td><a class="pure-button pure-button-primary" href="<?php echo add_query_arg( array( 'section' => 'mitra', 'edit' => $user->ID ), get_permalink() ); ?>">Edit</a></td>
                </tr>

              <?php $count++; }
            } ?>
          </tbody>
      </table>
  
      <?php endif; ?>
  
    <?php elseif( $section == "add-mitra" ) : ?>

    <?php 
    if ( isset( $_POST['submit-mitra'] ) ) {
      
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $full_name  =   sanitize_text_field( $_POST['full_name'] );
        $alamat     =   esc_textarea( $_POST['alamat'] );
        $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
        $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );
      
        registration_validation(
          $_POST['username'],
          $_POST['password'],
          $_POST['email'],
          $_POST['full_name'],
          $_POST['alamat'],
          $_POST['nomor_hp'],
          $_POST['nomor_wa']
        );
         
        // sanitize user form input
        global $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa, $staf;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $full_name  =   sanitize_text_field( $_POST['full_name'] );
        $alamat     =   esc_textarea( $_POST['alamat'] );
        $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
        $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );
        $staf       =   get_current_user_id();
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
          $username,
          $password,
          $email,
          $full_name,
          $alamat,
          $nomor_hp,
          $nomor_wa,
          $staf
        );
    }
    ?>
  
    <form method="post" action="#" class="form-horizontal">

            <div class="form-group row">
                <label for="full_name" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                  <input id="full_name" name="full_name" type="text" value="<?php echo ( isset( $_REQUEST['full_name'] ) ? $_REQUEST['full_name'] : null ); ?>" required class="form-control">
                </div>
            </div>
          
            <div class="form-group row">
                <label for="username" class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                  <input id="username" type="text" name="username" value="<?php echo ( isset( $_REQUEST['username'] ) ? $_REQUEST['username'] : null ); ?>" required class="form-control">
                  <span class="help-block">Username akan menjadi link mitra.</span>
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                  <input id="password" type="password" name="password" value="<?php echo ( isset( $_REQUEST['password'] ) ? $_REQUEST['password'] : null ); ?>" required class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input id="email" type="email" name="email" value="<?php echo ( isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : null ); ?>" required class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                  <textarea id="alamat" name="alamat" class="form-control"><?php echo ( isset( $_REQUEST['alamat'] ) ? $_REQUEST['alamat'] : null ); ?></textarea>
                </div>
            </div>
          
            <div class="form-group row">
                <label for="nomor_hp" class="col-sm-2 col-form-label">Nomor HP</label>
                <div class="col-sm-10">
                  <input id="nomor_hp" name="nomor_hp" type="text" value="<?php echo ( isset( $_REQUEST['nomor_hp'] ) ? $_REQUEST['nomor_hp'] : null ); ?>" class="form-control">
                </div>
            </div>
          
            <div class="form-group row">
                <label for="nomor_wa" class="col-sm-2 col-form-label">Nomor WA</label>
                <div class="col-sm-10">
                  <input id="nomor_wa" name="nomor_wa" type="text" value="<?php echo ( isset( $_REQUEST['nomor_wa'] ) ? $_REQUEST['nomor_wa'] : null ); ?>" class="form-control">
                </div>
            </div>
      
      
                              <div class="form-group row">
                              <p>
                                Rekening Bank (Untuk penerimaan komisi)
                                </p><br>
                              </div>
                              <div class="form-group row">
                                  <label for="nama_bank" class="col-sm-2 col-form-label">Nama Bank</label>
                                  <div class="col-sm-10">
                                    <input id="nama_bank" name="nama_bank" type="text" value="<?php echo ( isset( $_REQUEST['nama_bank'] ) ? $_REQUEST['nama_bank'] : null ); ?>" class="form-control">  
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label for="nomor_rekening" class="col-sm-2 col-form-label">Nomor Rekening</label>
                                  <div class="col-sm-10">
                                    <input id="nomor_rekening" name="nomor_rekening" type="text" value="<?php echo ( isset( $_REQUEST['nomor_rekening'] ) ? $_REQUEST['nomor_rekening'] : null ); ?>" class="form-control">  
                                  </div>
                              </div>

            <div class="form-group row">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                  <label>
                    <input id="cb" type="checkbox"> Kirim email pemberitahuan ke mitra.
                  </label>
                </div>
              </div>
            </div>
      
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit-mitra" class="btn btn-fullcolor btn--rounded">Submit</button>
            </div>

    </form>
  
    <?php else: ?>

    <?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
