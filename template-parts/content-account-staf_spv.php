<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option, $user_info, $section;
$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : 'profile';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <?php if( $section == "assignment" ) : ?>
        
        <table id="datatable1" class="table">
            <thead>
                <tr>
                    <th width="25">#</th>
                    <th>Tanggal</th>
                    <th>Customer</th>
                    <th>Kavling</th>
                    <th>Contact</th>
                    <th>Mitra</th>
                    <th>Staf</th>
                    <th>Status</th>
                    <th>Staf Follow-up</th>
                    <th><font style="color:black">My Follow-up</font></th>
                </tr>
            </thead>

            <tbody>
              <?php
              // WP_Query arguments
              $args = array(
                'post_type'              => array( 'participants' ),
                'post_status'            => array( 'publish' ),
                'posts_per_page'         => -1,
                'meta_key'              => 'spv',
                'meta_value'            => get_current_user_id(),
                'meta_value_num'        => get_current_user_id(),
                'meta_compare'          => '='
              );

              // The Query
              $query = new WP_Query( $args );

              // The Loop
              if ( $query->have_posts() ) {
                $count = 1;
                while ( $query->have_posts() ) {
                  $query->the_post(); ?>
                  <tr>
                      <td><?php echo $count; ?></td>
                      <td><?php echo get_the_date('d/m/Y'); ?></td>
                      <td><?php the_title(); ?></td>
                      <td><?php echo Mwt::get_field('kavling'); ?></td>
                      <td>
                        Email: <strong><?php echo Mwt::get_field('email'); ?></strong><br>
                        HP: <strong><?php echo Mwt::get_field('nomor_hp'); ?></strong>
                      </td>
                      <td><?php echo ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->display_name : ''; ?></td>
                      <td>
                        <?php 
//                         $staf = get_user_by( 'id', intval( get_user_meta( ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->ID : '', '_staf', true ) ) );
//                         if( $staf ) echo $staf->display_name;
                            echo ( !empty( Mwt::get_field('staf') ) ) ? Mwt::get_field('staf')->display_name : '';
                        ?>
                      </td>
                      <td><?php echo Mwt::get_field('status'); ?></td>
                      <td><?php echo Mwt::get_field( 'follow_up_result' ); ?></td>
                      <td>
                        <a href="#" class="spv-followup-result-input" data-type="textarea" data-pk="<?php the_ID(); ?>"><?php echo Mwt::get_field( 'spv_notes' ); ?></a>
                      </td>
                  </tr>
                  <?php
                  $count++;
                }
              }
              // Restore original Post Data
              wp_reset_postdata();
              ?>
            </tbody>
        </table>
        
        <?php else: ?>
        
        <?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
