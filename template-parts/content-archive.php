<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Landing Page
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  
  <div class="card card-plain card-blog">
    <div class="row">
      <div class="col-md-5">
        <?php if( has_post_thumbnail() ): ?>
        <div class="card-image">
          <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large' ); ?>" class="img img-raised rounded" style="width:100%;height:200px;object-fit:cover">
        </div>
        <?php endif; ?>
      </div>
      <div class="col-md-7">
        <h6 class="category text-info mt-3">
<?php the_category( ', ' ); ?>

</h6>
        <h3 class="card-title">
          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h3>
        <p class="card-description">
          <?php echo mb_strimwidth( strip_tags( get_the_content() ), 0, 65, '...' ); ?>
        </p>
        <p class="author">
          by <a href="#pablo"><b>Sarah Perez</b></a>, 2 days ago
        </p>
      </div>
    </div>
  </div>

</article><!-- #post-<?php the_ID(); ?> -->
