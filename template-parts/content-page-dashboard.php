<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Landing Page
 */

global $mwt, $mwt_option;
$user_info = get_userdata( get_current_user_id() );
$avatar_url = get_avatar_url( $user_info->user_email, array(
  'size'        => 300
) );
Mwt_Mjs::user_update_profile_action();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('dashboard-page'); ?>>
	<div class="page-header header-filter" data-parallax="true" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/examples/city.jpg');"></div>
	<div class="main main-raised">
		<div class="profile-content">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="profile">
							<div class="avatar">
								<img src="<?php echo $avatar_url; ?>" alt="Circle Image" class="img-circle img-responsive img-raised">
							</div>
							<div class="name">
								<h3 class="title">Welcome back, <?php echo $user_info->user_login; ?>!</h3>
								<h6><?php echo implode(', ', $user_info->roles); ?></h6>
							</div>
						</div>
					</div>
<!-- 					<div class="col-xs-2 follow">
						<button class="btn btn-fab btn-primary" rel="tooltip" title="Follow this user">
						<i class="material-icons">add</i>
						</button>
					</div> -->
				</div>
				<div class="description text-center">
					<p><?php echo $user_info->description; ?></p>
				</div>
        <!-- .entry-header -->
        <div class="description">
          <?php
            the_content();

            wp_link_pages( array(
              'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'mjslp' ),
              'after'  => '</div>',
            ) );
            ?>
        </div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="profile-tabs">
							<div class="nav-align-center">
								<ul class="nav nav-pills nav-pills-icons" role="tablist">
									<li class="active">
										<a href="#work" role="tab" data-toggle="tab">
										<i class="material-icons">palette</i>
										Follow Up
										</a>
									</li>
									<li>
										<a href="#connections" role="tab" data-toggle="tab">
										<i class="material-icons">people</i>
										My Profile
										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- End Profile Tabs -->
					</div>
				</div>
				<div class="tab-content">
					<div class="tab-pane active work" id="work">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
                <div class="table-responsive">
                  <table class="table">
                      <thead>
                          <tr>
                              <th class="text-center">#</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>Nomor HP/WA</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          // WP_Query arguments
                          $args = array(
                            'post_type'           => array( 'participants' ),
                            'post_status'         => array( 'publish' ),
                            'nopaging'            => true,
                            'posts_per_page'      => -1,
                            'meta_query'          => array(
                               'relation'   => 'AND',
                               array(
                                 'key'      => 'follow_up_by',
                                 'value'    => $user_info->ID,
                                 'type'     => 'NUMERIC',
                                 'compare'  => '='
                               ),
                               array(
                                 'key'      => 'follow_up_status',
                                 'value'    => 'Pending',
                                 'type'     => 'CHAR',
                                 'compare'  => '='
                               )
                            ), 
                          );

                          // The Query
                          $query = new WP_Query( $args );

                          // The Loop
                          if ( $query->have_posts() ) { $count = 1;
                            while ( $query->have_posts() ) {
                              $query->the_post(); ?>
                          <tr>
                              <td class="text-center"><?php echo $count; ?></td>
                              <td><?php echo get_field( 'nama' ); ?></td>
                              <td><?php echo get_field( 'email' ); ?></td>
                              <td><?php echo get_field( 'nomor_hp' ); ?></td>
                              <td class="td-actions text-right">
<!--                                   <button type="button" rel="tooltip" class="btn btn-info btn-round">
                                      <i class="material-icons">person</i>
                                  </button> -->
                                  <button type="button" class="btn btn-success btn-round follow-up-result-btn" data-id="<?php the_ID(); ?>">
                                      <i class="material-icons">edit</i>
                                  </button>

                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-<?php the_ID(); ?>" aria-hidden="true">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            <i class="material-icons">clear</i>
                                          </button>
                                          <h4 class="modal-title">Modal title</h4>
                                        </div>
                                        <div class="modal-body">
                                          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.
                                          </p>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-simple">Nice Button</button>
                                          <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                
                              </td>
                          </tr>
                          <tr id="participant<?php the_ID(); ?>" style="display:none" class="result-form-td success">
                            <td></td>
                            <th align="center">Follow Up Result</th>
                            <td colspan="4">
                              <form action="" method="post" class="follow-up-form" data-id="<?php the_ID(); ?>">
                                <div class="input-group" style="width:100%">
                                  <textarea class="form-control follow_up_result" name="follow_up_result" placeholder="Hasil follow up disini..."><?php echo get_field( 'follow_up_result' ); ?></textarea>
                                  <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default" type="button">Simpan</button>
                                  </span>
                                </div>
                              </form>
                            </td>
                          </tr>
                            <?php $count++;
                            }
                          }
                          // Restore original Post Data
                          wp_reset_postdata();
                          ?>
                      </tbody>
                  </table>
                </div>
							</div>
						</div>
					</div>
					<div class="tab-pane connections" id="connections">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <form role="form" id="profile-form" method="post">                  
                  <div class="form-group label-floating">
                    <label class="control-label">Username</label>
                    <input type="text" name="username" class="form-control" value="<?php echo $user_info->user_login; ?>" disabled>
                  </div>
                  <div class="form-group label-floating">
                    <label class="control-label">Full name</label>
                    <input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo $user_info->first_name; ?>">
                  </div>
                  <div class="form-group label-floating">
                    <label class="control-label">Email address</label>
                    <input type="email" name="email" class="form-control" value="<?php echo $user_info->user_email; ?>" disabled>
                  </div>
<!--                   <div class="form-group label-floating">
                    <label class="control-label">Phone</label>
                    <input type="text" name="phone" class="form-control"/>
                  </div> -->
                  <div class="form-group label-floating">
                    <label class="control-label">BIO</label>
                    <textarea name="bio" class="form-control" id="bio" rows="6"><?php echo $user_info->description; ?></textarea>
                  </div>
                  <div class="submit text-center">
                    <?php $wp_nonce = wp_create_nonce( 'mwt-nonce' ); ?>
                    <input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo $wp_nonce; ?>">
                    <input type="submit" class="btn btn-primary btn-raised btn-round" value="Simpan" />
                  </div>
                </form>
              </div>
            </div>


					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<!-- #post-<?php the_ID(); ?> -->

